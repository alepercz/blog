# Backend

Backend created with Strapi with data in MongoDB.

- data are taken from Strapi CMS
- or straight from MongoDB database (emoji-reactions, comments)
- I added more entities to article object for my needs (api/article/controllers/article.js)
