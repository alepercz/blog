const { sanitizeEntity } = require("strapi-utils");

module.exports = {
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */

  async findOne(ctx) {
    const { slug } = ctx.params;

    const entity = await strapi.services.article.findOne({ slug });
    return sanitizeEntity(entity, { model: strapi.models.article });
  },

  async find(ctx) {
    const entities = await strapi.services.article.find(ctx.query, [
      "category",
      "category.title",
      "category.slug",
      "category.image.url",
    ]);

    return entities.map((entity) =>
      sanitizeEntity(entity, { model: strapi.models.article })
    );
  },
};
