import Page from "../../components/page/Page";
import BasicPost from "../../components/basic/origin/blog/BasicPost";
import { getAll, getCategoryGroups } from "../api/api";
import { getStrapiMedia } from "../api/media";
import classes from "../../styles/Post.module.css";

const About = ({ article, allCategories, publicationDates }) => {
  const dates = publicationDates.map((date) => new Date(date));

  return (
    <Page
      pageTitle={article.title}
      categories={allCategories}
      dates={dates}
      seo={article.seo}
      isAboutMePage={true}
    >
      <BasicPost
        isPost={false}
        image={getStrapiMedia(article.image)}
        alt={article.altText}
        title={article.title}
        content={article.content}
        classContent={classes.content}
      />
    </Page>
  );
};

export async function getStaticProps() {
  const article = await getAll("/about-us");

  //for SidebarWidgets => CategoryGroups with array of posts of each category (to count their number)
  const allCategories = await getCategoryGroups();

  // for SidebarWidgets => Calendar to mark days in which post was published
  const posts = await getAll("/articles");
  const publicationDates = posts.map((post) => post.published_at);

  return {
    props: { article, allCategories, publicationDates },
  };
}

export default About;
