import ErrorPage from "../components/basic/origin/error/ErrorPage";
import Page from "../components/page/Page";

const NotFoundPage = () => {
  return (
    <Page>
      <ErrorPage
        image="/error-404.png"
        message="Nie znaleziono strony dla podanego adresu URL"
        styleMessage={{
          borderRadius: "100px",
          boxShadow: "var(--little-box-shadow)",
        }}
      />
    </Page>
  );
};

export default NotFoundPage;
