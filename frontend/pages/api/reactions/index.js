import dbConnect from "../../../utils/dbConnect";
import Reaction from "../../../models/Reaction";

dbConnect();

export default async function reactions(req, res) {
  const { method } = req;

  switch (method) {
    case "GET":
      try {
        const reactions = await Reaction.find({});
        res.status(200).json({ success: true, data: reactions });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "POST":
      try {
        const reaction = await Reaction.create(req.body);
        res.status(201).json({ success: true, data: reaction });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
}
