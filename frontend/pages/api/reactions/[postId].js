import dbConnect from "../../../utils/dbConnect";
import Reaction from "../../../models/Reaction";

dbConnect();

export default async function reactions(req, res) {
  const {
    query: { postId },
    method,
  } = req;

  switch (method) {
    case "GET":
      try {
        const reaction = await Reaction.findOne({ postID: postId });
        if (!reaction) {
          return res.status(400).json({ success: false });
        }
        res.status(200).json({ success: true, data: reaction });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "PUT":
      try {
        const reaction = await Reaction.findOneAndUpdate(
          { postID: postId },
          req.body,
          {
            new: true,
            runValidators: true,
          }
        );
        if (!reaction) {
          return res.status(400).json({ success: false });
        }
        res.status(200).json({ success: true, data: reaction });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "DELETE":
      try {
        const deleteReaction = await Reaction.deleteOne({ postID: postId });
        if (!deleteReaction) {
          return res.status(400).json({ success: false });
        }
        res.status(200).json({ success: true, data: {} });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
}
