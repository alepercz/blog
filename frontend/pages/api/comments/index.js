import dbConnect from "../../../utils/dbConnect";
import Comment from "../../../models/Comment";

dbConnect();

export default async function comments(req, res) {
  const { method } = req;

  switch (method) {
    case "GET":
      try {
        const comments = await Comment.find({});
        res.status(200).json({ success: true, data: comments });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "POST":
      try {
        const human = await validateHuman(req.body.token);
        if (!human) {
          res.status(400).json({ success: false });
          return;
        }
        const comment = await Comment.create(req.body);
        res.status(201).json({ success: true, data: comment });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "DELETE":
      try {
        const deleteAllComments = await Comment.deleteMany({});
        res.status(200).json({ success: true, data: {} });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
}

async function validateHuman(token) {
  const secret = process.env.RECAPTCHA_SECRET_KEY;
  const res = await fetch(
    `https://www.google.com/recaptcha/api/siteverify?secret=${secret}&response=${token}`,
    { method: "POST" }
  );
  const data = await res.json();
  return data.success;
}
