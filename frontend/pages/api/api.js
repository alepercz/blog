export function getStrapiURL(path = "") {
  return `${
    process.env.NEXT_PUBLIC_STRAPI_API_URL || "http://localhost:1337"
  }${path}`;
}

// Helper to make GET requests to Strapi
export async function fetchAPI(path) {
  const requestUrl = getStrapiURL(path);
  const response = await fetch(requestUrl);
  const data = await response.json();
  return data;
}

// helper to GET ALL from Strapi
// getAll('/articles');
export async function getAll(path) {
  return fetchAPI(path);
}

// helper to GET single Category by define Category Slug from Strapi
// getCategory("body");
export async function getCategory(categorySlug) {
  return fetchAPI(`/categories?slug=${categorySlug}`);
}

// helper to GET single Post by define Post Slug from Strapi
// getPost("beautiful-picture");
export async function getPost(articleSlug) {
  return fetchAPI(`/articles?slug=${articleSlug}`);
}

// helper to GET all POSTS by define Category Slug from Strapi
// getPostsByCategory("body");
export async function getPostsByCategorySortedBy(
  categorySlug,
  sortBy,
  sortType = "asc",
  start,
  limit
) {
  if (sortBy && sortType && (start || start == 0) && limit) {
    return fetchAPI(
      `/articles?category.slug=${categorySlug}&_sort=${sortBy}:${sortType}&_start=${start}&_limit=${limit}`
    );
  } else if (sortBy && sortType && !start && limit) {
    return fetchAPI(
      `/articles?category.slug=${categorySlug}&_sort=${sortBy}:${sortType}&_limit=${limit}`
    );
  } else if (sortBy && sortType && (start || start == 0) && !limit) {
    return fetchAPI(
      `/articles?category.slug=${categorySlug}&_sort=${sortBy}:${sortType}&_start=${start}`
    );
  } else if (sortBy && sortType) {
    return fetchAPI(
      `/articles?category.slug=${categorySlug}&_sort=${sortBy}:${sortType}`
    );
  } else {
    return fetchAPI(`/articles?category.slug=${categorySlug}`);
  }
}

// helper to GET all sorted Results from Strapi
// getResultsSortedBy("/articles", "published_at", "desc", 3, 0);
export async function getResultsSortedBy(
  path,
  sortBy,
  sortType = "asc",
  start,
  limit
) {
  if (sortBy && (start == 0 || start) && limit) {
    return fetchAPI(
      `${path}?_sort=${sortBy}:${sortType}&_start=${start}&_limit=${limit}`
    );
  } else if (sortBy && !start && limit) {
    return fetchAPI(`${path}?_sort=${sortBy}:${sortType}&_limit=${limit}`);
  } else if (sortBy && (start || start == 0) && !limit) {
    return fetchAPI(`${path}?_sort=${sortBy}:${sortType}&_start=${start}`);
  } else if (sortBy) {
    return fetchAPI(`${path}?_sort=${sortBy}:${sortType}`);
  }
}

// helper to GET all POSTS by define Category Slug from Strapi
// getPostsByCategory("body");
export async function getAuthorByID(authorID) {
  return fetchAPI(`/writers?id=${authorID}`);
}

export async function getCategoryGroups() {
  const categories = await getAll("/categories");

  const categoryGroups = await Promise.all(
    categories.map(async (category) => ({
      title: category.title,
      slug: category.slug,
      image: category.image,
      postsArr: await getPostsByCategorySortedBy(category.slug),
    }))
  );
  return categoryGroups;
}

export async function getTotalPostsByCategory(categorySlug) {
  const allPostsByCategory = await getPostsByCategorySortedBy(categorySlug);
  return allPostsByCategory.length;
}

export async function getTotalPosts() {
  const allPosts = await getAll("/articles");
  return allPosts.length;
}
