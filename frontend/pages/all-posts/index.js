import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Page from "../../components/page/Page";
import PostList from "../../components/post/postList/PostList";
import { getResultsSortedBy, getTotalPosts } from "../api/api";

const AllPosts = () => {
  const router = useRouter();

  const [partOfPosts, setPartOfPosts] = useState([]);
  const [totalPostsNum, setTotalPostsNum] = useState(0);
  const [startNum, setStartNum] = useState(0);
  const [maxPageNum, setMaxPageNum] = useState(0);

  const minPageNum = 1;
  const limitPerPage = 9;

  useEffect(() => {
    async function getTotal() {
      const total = await getTotalPosts();
      setTotalPostsNum(total);
    }
    getTotal();
  }, []);

  useEffect(() => {
    async function getPosts(start, limit) {
      const posts = await getResultsSortedBy(
        "/articles",
        "published_at",
        "desc",
        start,
        limit
      );
      setPartOfPosts(posts);
    }

    getPosts(startNum, limitPerPage);
  }, [startNum]);

  useEffect(() => {
    if (totalPostsNum) {
      setMaxPageNum(Math.ceil(totalPostsNum / limitPerPage));
    }
  }, [totalPostsNum]);

  useEffect(() => {
    const pageNum = Number(router.query.page);

    if (maxPageNum && pageNum > maxPageNum) {
      router.replace(`${router.pathname}?page=${maxPageNum}`);
    }
  }, [maxPageNum]);

  useEffect(() => {
    const pageNum = Number(router.query.page);
    if (!Boolean(pageNum)) {
      router.replace(`${router.pathname}?page=1`);
    }
    if (pageNum < minPageNum) {
      router.replace(`${router.pathname}?page=1`);
    } else {
      const start = (pageNum - 1) * limitPerPage;
      setStartNum(start);
    }
  }, [router.query.page]);

  return (
    <Page pageTitle="wszystkie posty">
      <PostList
        posts={partOfPosts}
        maxPageNum={maxPageNum}
        pathname={router.pathname}
        addCategoryName={true}
      />
    </Page>
  );
};

export default AllPosts;
