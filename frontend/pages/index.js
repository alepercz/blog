import RecentPostsSlider from "../components/homepage/RecentPostsSlider";
import CategoryList from "../components/homepage/CategoryList";
import Page from "../components/page/Page";
import PostBanner from "../components/post/postBanner/PostBanner";
import Button from "../components/ui/Button";
import BasicSubheader from "../components/basic/origin/layout/header/BasicSubheader";
import { getAll, getCategoryGroups, getResultsSortedBy } from "./api/api";
import Stripe from "stripe";

const Home = ({
  categoryGroups,
  publicationDates,
  mostRecentPosts,
  restOfPosts,
  categories,
  homepage,
  prices,
}) => {
  const dates = publicationDates.map((date) => new Date(date));

  return (
    <>
      <RecentPostsSlider posts={mostRecentPosts} />
      <CategoryList categories={categories} />
      <Page
        categories={categoryGroups}
        dates={dates}
        seo={homepage.seo}
        prices={prices}
      >
        <BasicSubheader
          title="Pozostałe posty"
          classMainSubheader="fw-light mx-auto"
        />
        {restOfPosts.map((post) => (
          <PostBanner key={post.id} post={post} />
        ))}
        <Button link="/all-posts?page=1">Wszystkie posty</Button>
      </Page>
    </>
  );
};

export async function getServerSideProps() {
  // for RecentPostsSlider with 3 recent posts
  const mostRecentPosts = await getResultsSortedBy(
    "/articles",
    "published_at",
    "desc",
    0,
    3
  );

  // to list rest of posts from all categories and sorted by date (only 3 on page, all posts displays by click the button)
  const restOfPosts = await getResultsSortedBy(
    "/articles",
    "published_at",
    "desc",
    3,
    3
  );

  const categories = await getAll("/categories");

  //for SidebarWidgets => CategoryGroups with array of posts of each category (to count their number)
  const categoryGroups = await getCategoryGroups();

  // for SidebarWidgets => Calendar to mark days in which post was published
  const posts = await getAll("/articles");
  const publicationDates = posts.map((post) => post.published_at);

  // for Seo
  const homepage = await getAll("/homepage");

  const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {
    apiVersion: "2020-08-27",
  });

  const prices = await stripe.prices.list({
    active: true,
    limit: 10,
    expand: ["data.product"],
  });

  return {
    props: {
      categoryGroups,
      publicationDates,
      mostRecentPosts,
      restOfPosts,
      categories,
      homepage,
      prices: prices.data,
    },
  };
}

export default Home;
