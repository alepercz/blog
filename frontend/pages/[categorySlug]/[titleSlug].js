import Stripe from "stripe";
import Page from "../../components/page/Page";
import Post from "../../components/post/post/Post";
import { getAll, getAuthorByID, getCategoryGroups, getPost } from "../api/api";

const PostPage = ({
  post,
  author,
  publicationDates,
  categoryGroups,
  prices,
}) => {
  const dates = publicationDates.map((date) => new Date(date));

  return (
    <Page
      pageTitle={post.category.title}
      pageLink={`/${post.category.slug}`}
      postTitle={post.title}
      categories={categoryGroups}
      dates={dates}
      seo={post.seo}
      article={true}
      prices={prices}
    >
      <Post post={post} author={author} />
    </Page>
  );
};

export async function getServerSideProps({ params }) {
  const { categorySlug, titleSlug } = params;

  const [post] = await getPost(titleSlug);
  const [author] = await getAuthorByID(post.author);

  // for SidebarWidgets => Calendar to mark days in which post was published
  const posts = await getAll("/articles");
  const publicationDates = posts.map((post) => post.published_at);

  //for SidebarWidgets => CategoryGroups with array of posts of each category (to count their number)
  const categoryGroups = await getCategoryGroups();

  const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {
    apiVersion: "2020-08-27",
  });

  const prices = await stripe.prices.list({
    active: true,
    limit: 10,
    expand: ["data.product"],
  });

  return {
    props: {
      post,
      author,
      publicationDates,
      categoryGroups,
      prices: prices.data,
    },
  };
}

export default PostPage;
