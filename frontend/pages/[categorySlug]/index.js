import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Page from "../../components/page/Page";
import PostList from "../../components/post/postList/PostList";
import {
  getCategory,
  getPostsByCategorySortedBy,
  getTotalPostsByCategory,
} from "../api/api";

const PostsByCategory = () => {
  const router = useRouter();

  const [categorySlug, setCategorySlug] = useState("");
  const [aboutPage, setAboutPage] = useState({});
  const [postsByCategory, setPostsByCategory] = useState([]);

  const [totalPostsNum, setTotalPostsNum] = useState(0);
  const [startNum, setStartNum] = useState(0);
  const [maxPageNum, setMaxPageNum] = useState(0);

  const minPageNum = 1;
  const limitPerPage = 9;

  useEffect(() => {
    async function getAboutPageInfo(categorySlug) {
      const [infoAboutPage] = await getCategory(categorySlug);
      setAboutPage(infoAboutPage);
    }

    async function getPosts(categorySlug, start, limit) {
      const posts = await getPostsByCategorySortedBy(
        categorySlug,
        "published_at",
        "desc",
        start,
        limit
      );
      if (posts.length) {
        setPostsByCategory(posts);
      } else {
        // there is no category without post -> incorrect url
        router.replace("/");
      }
    }

    async function getTotal(categorySlug) {
      const total = await getTotalPostsByCategory(categorySlug);
      setTotalPostsNum(total);
    }

    if (router.query.categorySlug !== categorySlug) {
      getAboutPageInfo(router.query.categorySlug);
      getTotal(router.query.categorySlug);
      setCategorySlug(router.query.categorySlug);
    }

    getPosts(router.query.categorySlug, startNum, limitPerPage);
  }, [router.query.categorySlug, startNum]);

  useEffect(() => {
    if (totalPostsNum) {
      setMaxPageNum(Math.ceil(totalPostsNum / limitPerPage));
    }
  }, [totalPostsNum]);

  useEffect(() => {
    const pageNum = Number(router.query.page);

    if (maxPageNum && pageNum > maxPageNum) {
      router.replace(`/${router.query.categorySlug}?page=${maxPageNum}`);
    }
  }, [maxPageNum]);

  useEffect(() => {
    const pageNum = Number(router.query.page);

    if (!Boolean(pageNum)) {
      router.replace(`/${router.query.categorySlug}?page=1`);
    }

    if (pageNum < minPageNum) {
      router.replace(`/${router.query.categorySlug}?page=1`);
    } else {
      const start = (pageNum - 1) * limitPerPage;
      setStartNum(start);
    }
  }, [router.query.page]);

  return (
    <Page pageTitle={aboutPage?.title} seo={aboutPage?.seo}>
      <PostList
        posts={postsByCategory}
        maxPageNum={maxPageNum}
        pathname={router.query.categorySlug}
        addCategoryName={false}
      />
    </Page>
  );
};

export default PostsByCategory;
