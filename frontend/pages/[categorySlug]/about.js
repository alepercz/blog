import Page from "../../components/page/Page";
import BasicPost from "../../components/basic/origin/blog/BasicPost";
import { getAll, getCategory, getCategoryGroups } from "../api/api";
import { getStrapiMedia } from "../api/media";
import classes from "../../styles/Post.module.css";

const AboutCategory = ({ article, allCategories, publicationDates }) => {
  const dates = publicationDates.map((date) => new Date(date));

  return (
    <Page
      pageTitle={article.title}
      pageLink={`/${article.slug}`}
      postTitle="o kategorii"
      categories={allCategories}
      dates={dates}
      seo={article.seo}
    >
      <BasicPost
        isPost={false}
        image={getStrapiMedia(article.image)}
        alt={article.altText}
        title={article.title}
        content={article.content}
        classContent={classes.content}
      />
    </Page>
  );
};

export async function getStaticPaths() {
  const categories = await getAll("/categories");

  const paths = categories.map((category) => ({
    params: { categorySlug: category.slug },
  }));

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const { categorySlug } = params;
  const [article] = await getCategory(categorySlug);

  //for SidebarWidgets => CategoryGroups with array of posts of each category (to count their number)
  const allCategories = await getCategoryGroups();

  // for SidebarWidgets => Calendar to mark days in which post was published
  const posts = await getAll("/articles");
  const publicationDates = posts.map((post) => post.published_at);

  return {
    props: { article, allCategories, publicationDates },
  };
}

export default AboutCategory;
