import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";

import App from "next/app";
import Head from "next/head";
import Layout from "../components/layout/Layout";
import { createContext } from "react";
import { getStrapiMedia } from "./api/media";
import { fetchAPI } from "./api/api";

// Store Strapi Global object in context
export const GlobalContext = createContext({});

const MyApp = ({ Component, pageProps }) => {
  const { blog, footer, menu } = pageProps;

  return (
    <>
      <Head>
        <link rel="shortcut icon" href={getStrapiMedia(blog.logo)} />

        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Raleway:wght@200;400;700&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Caveat&display=swap"
          rel="stylesheet"
        />
      </Head>
      <GlobalContext.Provider value={blog}>
        <Layout blog={blog} footer={footer} menu={menu}>
          <Component {...pageProps} />
        </Layout>
      </GlobalContext.Provider>
    </>
  );
};

// getInitialProps disables automatic static optimization for pages that don't
// have getStaticProps. So article, category and home pages still get SSG.
// Hopefully we can replace this with getStaticProps once this issue is fixed:
// https://github.com/vercel/next.js/discussions/10949
MyApp.getInitialProps = async (ctx) => {
  // Calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(ctx);
  // Fetch global site settings from Strapi
  const blog = await fetchAPI("/blog");

  let menu = [
    {
      title: "home",
      pathname: "/",
    },
  ];
  const categories = await fetchAPI("/categories");
  const categoryTitlesAndSlugs = categories.map((category) => ({
    title: category.title,
    slug: category.slug,
    pathname: `/${category.slug}?page=1`,
  }));

  const aboutMePage = await fetchAPI("/about-us");
  const aboutMe = {
    title: aboutMePage.title,
    pathname: `/${aboutMePage.slug}`,
  };
  menu.push(...categoryTitlesAndSlugs, aboutMe);

  const footer = await fetchAPI("/footer");

  // Pass the data to our page via props
  return { ...appProps, pageProps: { blog, footer, menu } };
};

export default MyApp;
