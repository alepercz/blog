import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Page from "../../components/page/Page";
import BasicPicture from "../../components/basic/origin/ui/image/BasicPicture";
import Button from "../../components/ui/Button";

const getRandomIntInclusive = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const Donation = () => {
  const [paymentStatus, setPaymentStatus] = useState();
  const [isMessage, setIsMessage] = useState();
  const [imageNum, setImageNum] = useState(getRandomIntInclusive(1, 3));
  const router = useRouter();

  useEffect(() => {
    const query = new URLSearchParams(window.location.search);

    if (!query.get("payment_intent")) {
      router.replace(`/`);
    } else {
      setIsMessage(Number(query.get("message")));
      setPaymentStatus(query.get("redirect_status"));
      router.replace(`/donation`);
    }
  }, []);

  let imagePath = "/donation";

  if (paymentStatus === "succeeded") {
    imagePath += isMessage
      ? `/message/${imageNum}.png`
      : `/success/${imageNum}.png`;
  } else {
    imagePath += `/fail/${imageNum}.png`;
  }

  return (
    <Page>
      <div
        style={{ height: "400px" }}
        className="d-flex justify-content-center align-items-center"
      >
        <BasicPicture
          type="image"
          picture={imagePath}
          stylePicture={{ width: "300px", height: "300px" }}
        />
        <div className="px-5">
          {paymentStatus === "succeeded" ? (
            isMessage ? (
              <div>
                <h1>Oooo! Dziękuję za wiadomość!</h1>
                <h4>Odczytam ją tak szybko jak tylko będę mogła.</h4>
                <h4 className="fw-bolder">
                  Twoje wsparcie wiele dla mnie znaczy!
                </h4>
              </div>
            ) : (
              <div>
                <h1>WoW! Dziękuję!</h1>
                <h4>Motywujesz mnie do dalszej pracy.</h4>
                <h4 className="fw-bolder">
                  Twoje wsparcie wiele dla mnie znaczy!
                </h4>
              </div>
            )
          ) : (
            <div>
              <h1>Ooops! Coś poszło nie tak...</h1>
              <h4>Płatność nie przeszła pomyślnie.</h4>
              <h4 className="fw-bolder">Spróbuj ponownie później.</h4>
            </div>
          )}
          <Button link="/">WRÓĆ NA STRONĘ GŁÓWNĄ</Button>
        </div>
      </div>
    </Page>
  );
};

export default Donation;
