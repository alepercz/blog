import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Page from "../../components/page/Page";
import PostList from "../../components/post/postList/PostList";
import ErrorPage from "../../components/basic/origin/error/ErrorPage";
import { getResultsSortedBy } from "../api/api";

const Search = () => {
  const router = useRouter();
  const [searchQuery, setSearchQuery] = useState("");
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    async function getPosts() {
      const allPosts = await getResultsSortedBy(
        "/articles",
        "published_at",
        "desc"
      );
      setPosts(allPosts);
    }
    getPosts();

    const { search } = window.location;
    const query = new URLSearchParams(search).get("results");
    if (!query) {
      router.replace(`/${router.pathname}?results=all`);
    }
    setSearchQuery(query);
  }, []);

  const filterPosts = (posts, query) => {
    if (!query || query === "all") {
      return posts;
    }
    return posts.filter((post) => {
      const postName = post.title.toLowerCase();
      return postName.includes(query.toLowerCase());
    });
  };

  const filteredPosts = filterPosts(posts, searchQuery);

  return (
    <Page pageTitle="wyszukane posty">
      {filteredPosts.length ? (
        <PostList posts={filteredPosts} addCategoryName={true} />
      ) : (
        <ErrorPage
          image="/search.png"
          message="Nie znaleziono postów dla szukanej frazy"
          styleMessage={{
            borderRadius: "100px",
            boxShadow: "var(--little-box-shadow)",
          }}
        />
      )}
    </Page>
  );
};

export default Search;
