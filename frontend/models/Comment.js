const mongoose = require("mongoose");

const CommentSchema = new mongoose.Schema({
  nickname: {
    type: String,
    required: true,
    trim: true,
    maxLength: [20, "Podpis nie może być dłuższy niż 20 znaków."],
  },
  comment: {
    type: String,
    required: true,
    maxLength: [1000, "Komentarz nie może przekraczać 1000 znaków."],
  },
  token: String,
  postID: mongoose.Schema.Types.ObjectId,
  created_at: { type: Date, default: Date.now },
});

module.exports =
  mongoose.models.Comment || mongoose.model("Comment", CommentSchema);
