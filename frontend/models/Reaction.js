const mongoose = require("mongoose");

const ReactionSchema = new mongoose.Schema({
  titleSlug: {
    type: String,
    unique: true,
  },
  categorySlug: String,
  reactionsCount: [{}],
  postID: {
    type: mongoose.Schema.Types.ObjectId,
    unique: true,
  },
});

module.exports =
  mongoose.models.Reaction || mongoose.model("Reaction", ReactionSchema);
