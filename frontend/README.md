# Frontend

Frontend in NextJS with CMS in Strapi.

- payment option (suggested amounts or user's amount), possibility to pay by Przelewy24 or Card (Stripe)
- comment section (write and display comments, comments save in MongoDB)
- emoji reactions on post and their count (user's reaction save in LocalStorage, all reactions save in MongoDB)
- search bar to search posts by keywords includes in titles
- calendar with marked publication dates.
- SEO optimization for every page (meta-title, meta-description, alt-text)
- Slider with 3 latest posts
- Previous / Next post pagination
- Read-also with related posts
- Bootstrap customized components
- basic folder with my reusable components for any project
- all images are taken from unsplash.com
- all icons and stickers are taken from flaticon.com
