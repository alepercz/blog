import { useRouter } from "next/router";
import { Container, Row, Col } from "react-bootstrap";
import BasicCategoryCard from "../basic/origin/ui/card/categoryCard/BasicCategoryCard";
import { getStrapiMedia } from "../../pages/api/media";
import classes from "./CategoryList.module.css";

const CategoryList = ({ categories }) => {
  const router = useRouter();

  return (
    <div
      className="mb-4 py-4 py-md-4"
      style={{ backgroundColor: "var(--color-tetriary-dark)" }}
    >
      <hr className="m-0" />
      <h2 className="fw-light text-center my-3 my-sm-4">
        Zapoznaj się z kategoriami
      </h2>
      <Container
        style={{ maxWidth: "1200px" }}
        className="mb-3 mb-sm-4 pb-md-5"
      >
        <Row className="align-items-center">
          {categories.map((category) => (
            <Col xs={12} md={4} key={category.id}>
              <BasicCategoryCard
                image={getStrapiMedia(category.image)}
                title={category.title.toUpperCase()}
                onClick={() => router.push(`/${category.slug}/about`)}
                classCaption={classes.cardCaption}
                classTitle={classes.cardTitle}
              />
            </Col>
          ))}
        </Row>
      </Container>
      <hr className="m-0" />
    </div>
  );
};

export default CategoryList;
