import BasicSlider from "../basic/origin/ui/slider/BasicSlider";

const RecentPostsSlider = ({ posts }) => {
  return (
    <BasicSlider
      posts={posts}
      styleCaption={{ textShadow: "var(--text-shadow)", zIndex: "999" }}
      styleSubtitle={{ backgroundColor: "var(--color-secondary)" }}
    />
  );
};

export default RecentPostsSlider;
