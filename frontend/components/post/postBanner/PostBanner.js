import { useRouter } from "next/router";
import BasicCard from "../../basic/origin/ui/card/basicCard/BasicCard";
import BasicFooterArrow from "../../../components/basic/origin/layout/footer/BasicFooterArrow";
import { option2 } from "../../../utils/dateFormatOptions";
import { getStrapiMedia } from "../../../pages/api/media";
import { useWindowSize } from "../../basic/origin/hook/useWindowSize";
import classes from "./PostBanner.module.css";

const PostBanner = ({ post }) => {
  const router = useRouter();
  const { width } = useWindowSize();

  return (
    <BasicCard
      vertical={width < 768}
      src={getStrapiMedia(post.image)}
      alt={post.title}
      header={post.category.title.toUpperCase()}
      title={post.title}
      subtitle={new Date(post.published_at).toLocaleDateString(
        "pl-PL",
        option2
      )}
      text={post.description}
      footer={<BasicFooterArrow text="Więcej" />}
      styleCard={
        width < 768
          ? {
              maxWidth: "360px",
              margin: "50px auto",
              cursor: "default",
            }
          : {
              minHeight: "300px",
              maxWidth: "95%",
              margin: "30px auto",
              cursor: "default",
            }
      }
      classPicture={classes.picture}
      classBody="m-2"
      classHeader={classes.header}
      classTitle={classes.title}
      classSubtitle="my-2"
      styleSubtitle={{ fontWeight: "200", color: "var(--color-secondary)" }}
      classText="text-justify"
      classFooter={classes.footer}
      onClickCard={() => router.push(`/${post.category.slug}/${post.slug}`)}
      onClickHeader={() => router.push(`/${post.category.slug}?page=1`)}
    />
  );
};
export default PostBanner;
