import { useEffect, useState } from "react";
import BasicCommentSection from "../../basic/origin/blog/postComments/BasicCommentSection";
import { getPostComments } from "../../../utils/commentsHelpers";
import { option4 } from "../../../utils/dateFormatOptions";
import classes from "./CommentSection.module.css";

const CommentSection = ({ postId }) => {
  const [postComments, setPostComments] = useState([]);

  useEffect(() => {
    async function get(postID) {
      const comments = await getPostComments(postID);
      setPostComments(comments);
    }
    get(postId);
  }, [postId]);

  return (
    <>
      <BasicCommentSection
        // header
        id="comments"
        header={
          postComments.length == 0 ? "Dodaj pierwszy komentarz!" : "Komentarze"
        }
        subheader="Jestem ciekawa Twojego zdania"
        classMainSubheader="fw-light mx-auto"
        classSubheader="fw-light mx-auto"
        // form
        btnText="Dodaj"
        labelComment={
          postComments.length == 0
            ? "Rozpocznij dyskusję"
            : "Dołącz do dyskusji"
        }
        labelNickname={"Podpis"}
        postId={postId}
        recaptchaKey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
        setPostComments={setPostComments}
        classLabelComment={classes.labelComment}
        classComment={classes.comment}
        classLabelNickname={classes.labelNickname}
        classNickname={classes.nickname}
        classBtn={classes.btn}
        // comments
        postComments={postComments}
        styleHrLine={{ color: "var(--color-secondary)" }}
        picture="/user.png"
        dateStyleOption={option4}
        styleCommentNickname={{ color: "var(--color-primary)" }}
        styleCommentCard={{
          borderRadius: "90px",
          margin: "15px auto",
          cursor: "default",
          boxShadow: "var(--little-box-shadow)",
        }}
        classCommentPicture="position-relative my-auto ms-4 me-1 me-sm-2"
        styleCommentPicture={{
          height: "50px",
          width: "50px",
        }}
        styleCommentSubtitle={{
          fontSize: ".9rem",
          color: "var(--color-secondary)",
          marginTop: "-8px",
        }}
        classCommentText="mt-1 me-sm-4 pe-sm-5"
      />
    </>
  );
};

export default CommentSection;
