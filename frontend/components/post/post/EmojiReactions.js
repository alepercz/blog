import BasicEmojiReactions from "../../basic/origin/features/emojiReactions/BasicEmojiReactions";

const EmojiReactions = ({ postId, postSlug, categorySlug }) => {
  return (
    <BasicEmojiReactions
      postId={postId}
      postSlug={postSlug}
      postCategorySlug={categorySlug}
      reactionTitle="Co myślisz o tym poście?"
      classContainer="rounded-3"
      styleContainer={{
        background:
          "linear-gradient(180deg,rgba(198, 182, 255, 0.5) 0%,rgba(255, 255, 255, 1) 70%)",
        margin: "75px auto",
      }}
      styleEmoji={{ color: "var(--color-primary)", fontSize: "50px" }}
      styleEmojiMatch={{
        color: "var(--color-secondary)",
        fontSize: "50px",
      }}
      styleNumber={{ color: "var(--color-tetriary-dark)" }}
      styleNumberMatch={{ color: "var(--color-secondary)" }}
    />
  );
};

export default EmojiReactions;
