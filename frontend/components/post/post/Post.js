import BasicPost from "../../basic/origin/blog/BasicPost";
import EmojiReactions from "./EmojiReactions";
import PrevNextPost from "./prevNextPost/PrevNextPost";
import ReadAlso from "./readAlso/ReadAlso";
import CommentSection from "./CommentSection";
import { getStrapiMedia } from "../../../pages/api/media";
import classes from "../../../styles/Post.module.css";

const Post = ({ post, author }) => {
  return (
    <>
      <BasicPost
        image={getStrapiMedia(post.image)}
        alt={post.altText}
        category={post.category.title}
        author={author.name}
        date={post.published_at}
        opinion="Twoja opinia"
        title={post.title}
        content={post.content}
        classCategory={classes.category}
        classBody={classes.body}
        classOpinion={classes.opinion}
        classContent={classes.content}
      />
      <EmojiReactions
        postId={post.id}
        postSlug={post.slug}
        categorySlug={post.category.slug}
      />
      <PrevNextPost
        postId={post.id}
        postSlug={post.slug}
        categorySlug={post.category.slug}
      />
      <ReadAlso postId={post.id} categorySlug={post.category.slug} />
      <CommentSection postId={post.id} />
    </>
  );
};
export default Post;
