import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import PrevNextPage from "../../../basic/origin/blog/postPagination/PrevNextPage";
import { getAll, getPostsByCategorySortedBy } from "../../../../pages/api/api";
import { getStrapiMedia } from "../../../../pages/api/media";
import classes from "./ReadAlso.module.css";

const ReadAlso = ({ postId, categorySlug }) => {
  const [relatedPosts, setRelatedPosts] = useState("");
  const router = useRouter();

  const previousPost = relatedPosts[0];
  const nextPost = relatedPosts[1];

  useEffect(() => {
    async function get(categorySlug) {
      const categories = await getAll("/categories");
      const allCategories = categories.map((category) => category.slug);
      const categoriesWithoutCurrent = allCategories.filter(
        (category) => category !== categorySlug
      );

      let postsThatMayInterestYou = [];

      for (let i = 0; i < categoriesWithoutCurrent.length; i++) {
        const posts = await getPostsByCategorySortedBy(
          categoriesWithoutCurrent[i],
          "published_at",
          "desc"
        );
        postsThatMayInterestYou.push(posts[0]);
      }
      setRelatedPosts(postsThatMayInterestYou);
    }
    setRelatedPosts("");
    get(categorySlug);
  }, [postId]);

  return (
    relatedPosts && (
      <PrevNextPage
        header="Przeczytaj także"
        classMainSubheader="fw-light mx-auto"
        prevHeader={previousPost.category.title.toUpperCase()}
        prevTitle={previousPost.title}
        prevImage={getStrapiMedia(previousPost.image)}
        prevAlt={previousPost.altText}
        prevLink={() =>
          router.push(`/${previousPost.category.slug}/${previousPost.slug}`)
        }
        nextHeader={nextPost.category.title.toUpperCase()}
        nextTitle={nextPost.title}
        nextImage={getStrapiMedia(nextPost.image)}
        nextAlt={nextPost.altText}
        nextLink={() =>
          router.push(`/${nextPost.category.slug}/${nextPost.slug}`)
        }
        classContainer="px-0 px-sm-4"
        classCard={classes.prevNextCard}
        styleBody={{ height: "150px" }}
        classHeader="bg-transparent"
        styleHeader={{
          borderColor: "var(--color-secondary)",
          color: "var(--color-secondary)",
        }}
        styleTitle={{ fontWeight: "200" }}
        stylePicturePrev={{ height: "150px" }}
        stylePictureNext={{ height: "150px" }}
      />
    )
  );
};
export default ReadAlso;
