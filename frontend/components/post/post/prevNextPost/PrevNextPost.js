import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import PrevNextPage from "../../../basic/origin/blog/postPagination/PrevNextPage";
import { getPostsByCategorySortedBy } from "../../../../pages/api/api";
import { getStrapiMedia } from "../../../../pages/api/media";
import { useWindowSize } from "../../../basic/origin/hook/useWindowSize";
import classes from "./PrevNextPost.module.css";

const PrevNextPost = ({ postId, postSlug, categorySlug }) => {
  const [previousPost, setPreviousPost] = useState("");
  const [nextPost, setNextPost] = useState("");
  const router = useRouter();
  const { width } = useWindowSize();

  useEffect(() => {
    async function get(categorySlug, titleSlug) {
      const postsByCategory = await getPostsByCategorySortedBy(
        categorySlug,
        "published_at",
        "desc"
      );
      const index = postsByCategory.findIndex(
        (post) => post.slug === titleSlug
      );

      if (index > 0) {
        setPreviousPost(postsByCategory[index - 1]);
      }

      if (index < postsByCategory.length - 1) {
        setNextPost(postsByCategory[index + 1]);
      }
    }
    setPreviousPost("");
    setNextPost("");
    get(categorySlug, postSlug);
  }, [postId]);

  return (
    <PrevNextPage
      prevHeader="Poprzedni Post"
      prevTitle={previousPost.title}
      prevImage={previousPost && getStrapiMedia(previousPost.image)}
      prevAlt={previousPost.altText}
      prevLink={() =>
        router.push(`/${previousPost.category.slug}/${previousPost.slug}`)
      }
      nextHeader="Następny Post"
      nextTitle={nextPost.title}
      nextImage={nextPost && getStrapiMedia(nextPost.image)}
      nextAlt={nextPost.altText}
      nextLink={() =>
        router.push(`/${nextPost.category.slug}/${nextPost.slug}`)
      }
      imgLeftNext={false}
      classContainer="px-sm-5 my-sm-0 py-sm-2"
      classColPrev="ps-3 pe-0 pe-md-2"
      classColNext="pe-3 ps-0 ps-md-2"
      classCard={classes.prevNextCard}
      styleCard={{ height: `${width < 576 ? "fit-content" : "125px"}` }}
      classBodyPrev="pe-0 text-start"
      classBodyNext="ps-0 text-end"
      classHeader="bg-transparent p-0 pb-1"
      styleHeader={{
        fontSize: ".9rem",
        fontWeight: "700",
        color: "lightgrey",
        borderColor: "var(--color-primary)",
      }}
      classTitle="py-2"
      styleTitle={{
        fontSize: "1rem",
        fontWeight: "200",
      }}
      classPicture="position-relative my-0 rounded-circle overflow-hidden"
      stylePicturePrev={{
        height: `${width < 576 ? "75px" : "105%"}`,
        width: `${width < 576 ? "75px" : "130px"}`,
        marginLeft: `${width < 576 ? "-55px" : "-70px"}`,
      }}
      stylePictureNext={{
        height: `${width < 576 ? "75px" : "105%"}`,
        width: `${width < 576 ? "75px" : "130px"}`,
        marginRight: `${width < 576 ? "-55px" : "-70px"}`,
      }}
    />
  );
};
export default PrevNextPost;
