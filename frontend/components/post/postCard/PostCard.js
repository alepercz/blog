import { useRouter } from "next/router";
import { getStrapiMedia } from "../../../pages/api/media";
import BasicCard from "../../basic/origin/ui/card/basicCard/BasicCard";
import BasicFooterArrow from "../../basic/origin/layout/footer/BasicFooterArrow";
import { option2 } from "../../../utils/dateFormatOptions";
import classes from "./PostCard.module.css";

const PostCard = ({ post, addCategoryName }) => {
  const router = useRouter();

  return (
    <BasicCard
      src={getStrapiMedia(post.image)}
      alt={post.title}
      header={addCategoryName ? post.category.title.toUpperCase() : ""}
      title={post.title}
      subtitle={new Date(post.published_at).toLocaleDateString(
        "pl-PL",
        option2
      )}
      text={post.description}
      footer={<BasicFooterArrow text="Czytaj dalej" />}
      classCard={classes.card}
      classPicture={classes.image}
      classBody={classes.body}
      classHeader={classes.header}
      classTitle={classes.title}
      classSubtitle={classes.subtitle}
      classText={classes.text}
      classFooter={classes.footer}
      onClickCard={() => router.push(`/${post.category.slug}/${post.slug}`)}
      onClickHeader={() => router.push(`/${post.category.slug}?page=1`)}
    />
  );
};

export default PostCard;
