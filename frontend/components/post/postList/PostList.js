import { Container, Row, Col } from "react-bootstrap";
import PostCard from "../postCard/PostCard";
import Pagination from "../../ui/Pagination";

const PostList = ({ posts, addCategoryName, maxPageNum, pathname }) => {
  return (
    <Container className="my-5">
      <Row>
        {posts.length !== 0 &&
          posts.map((post) => (
            <Col md={6} lg={4} key={post.id} style={{ minHeight: "500px" }}>
              <PostCard post={post} addCategoryName={addCategoryName} />
            </Col>
          ))}
        <Pagination maxPageNum={maxPageNum} pathname={pathname} />
      </Row>
    </Container>
  );
};

export default PostList;
