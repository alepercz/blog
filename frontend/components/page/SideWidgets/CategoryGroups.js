import { useRouter } from "next/router";
import BasicBadge from "../../basic/origin/ui/card/BasicBadge";
import { getStrapiMedia } from "../../../pages/api/media";

const CategoryGroups = ({ categories }) => {
  const router = useRouter();

  return categories.map((category) => (
    <BasicBadge
      key={category.slug}
      id="cardBodyCategoryGroup"
      title={category.title.toUpperCase()}
      badge={category.postsArr.length}
      onClick={() => router.push(`/${category.slug}?page=1`)}
      pillShape={true}
      classCard="my-1 mx-3"
      classBody="d-flex align-items-center justify-content-between rounded-1"
      styleBody={{
        backgroundImage: `radial-gradient(circle, rgba(255,255,255,0) 40%, var(--color-base) 95%), url(${getStrapiMedia(
          category.image
        )})`,
        backgroundSize: "cover",
        backgroundPositionY: "-90px",
      }}
      classTitle="m-0 fw-lighter text-white"
      classBadge="bg-white"
      styleBadge={{ color: "var(--color-primary)" }}
    />
  ));
};

export default CategoryGroups;
