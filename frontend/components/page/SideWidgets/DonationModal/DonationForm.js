import { useState, useEffect } from "react";
import DonationButton from "../DonationButton/DonationButton";
import classes from "./DonationForm.module.css";

const DonationForm = ({ setPaymentStep, amountState, setMessage }) => {
  const [isDisabled, setIsDisabled] = useState(false);
  const [amount, setAmount] = amountState;

  useEffect(() => {
    if (amount < 5) {
      setIsDisabled(true);
    } else {
      setIsDisabled(false);
    }
  }, [amount]);

  const handleAmountChange = (e) => {
    const inputAmount = Math.floor(e.target.value);
    setAmount(inputAmount);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setPaymentStep(true);
  };
  return (
    <>
      <hr />
      <div className="text-center py-3">
        <h5>
          Możesz również wprowadzić{" "}
          <span className={classes.highlightSecondary}>własną wartość</span>
        </h5>
        <p>
          i dołączyć do niej{" "}
          <span className={classes.highlightPrimary}>wiadomość do mnie!</span>
        </p>
      </div>
      <form
        onSubmit={handleSubmit}
        className="d-flex flex-column m-auto"
        style={{ width: "500px" }}
      >
        <textarea
          cols="50"
          rows="4"
          placeholder="Rozpocznij wiadomość"
          className={classes.message}
          onChange={(e) => {
            setMessage(e.target.value);
          }}
        ></textarea>
        <div className="d-flex align-items-center">
          <label htmlFor="amount">
            <input
              id="amount"
              type="number"
              min="5"
              placeholder="10"
              className={classes.amount}
              onChange={handleAmountChange}
            />{" "}
            PLN
          </label>
          <span className={classes.error}>
            {isDisabled && amount > 0 && "Kwota minimalna to 5,00 zł."}
          </span>
        </div>
        <DonationButton type="submit" text="PŁACĘ" disabled={isDisabled} />
      </form>
    </>
  );
};

export default DonationForm;
