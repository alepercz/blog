import BasicPicture from "../../../basic/origin/ui/image/BasicPicture";
import { loadStripe } from "@stripe/stripe-js";
import { createCheckoutSession } from "next-stripe/client";
import classes from "./QuickTip.module.css";

const QuickTip = ({ prices }) => {
  const handlePrice = async (priceID) => {
    const session = await createCheckoutSession({
      success_url: window.location.href,
      cancel_url: window.location.href,
      line_items: [{ price: priceID, quantity: 1 }],
      payment_method_types: ["p24", "card"],
      mode: "payment",
    });
    const stripe = await loadStripe(process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY);

    if (stripe) {
      stripe.redirectToCheckout({
        sessionId: session.id,
      });
    }
  };

  return (
    <>
      <h5 className="text-center py-3">
        <span className={classes.important1}>Dobro wraca!</span> Oto moje
        propozycje prezentów{" "}
        <span className={classes.important2}>dla Ciebie!</span>
      </h5>
      <div className="d-flex justify-content-evenly">
        {prices.map((price) => (
          <div
            key={price.id}
            className={classes.gift}
            onClick={() => handlePrice(price.id)}
          >
            <BasicPicture
              type="img"
              picture={price.product.images[0]}
              stylePicture={{ width: "100px", height: "100px" }}
            />
            <p className={classes.giftDescription}>{price.product.name}</p>
            <span className={classes.giftPrice}>
              {price.unit_amount / 100} zł
            </span>
          </div>
        ))}
      </div>
    </>
  );
};

export default QuickTip;
