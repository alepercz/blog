import { useState, useEffect } from "react";
import BasicModal from "../../../basic/origin/ui/modal/BasicModal";
import StripeContainer from "../../../basic/origin/features/payment/StripeContainer";
import QuickTip from "./QuickTip";
import DonationForm from "./DonationForm";
import { FaHandHoldingUsd } from "react-icons/fa";
import { useWindowSize } from "../../../basic/origin/hook/useWindowSize";
import classes from "./DonationModal.module.css";

const DonationModal = ({ show, setModalShow, prices }) => {
  const [paymentStep, setPaymentStep] = useState(false);
  const [amount, setAmount] = useState(0);
  const [message, setMessage] = useState("");

  const { width } = useWindowSize();

  useEffect(() => {
    if (width < 992) {
      setModalShow(false);
      setAmount(0);
      setMessage("");
      setPaymentStep(false);
    }
  }, [width]);

  return (
    <BasicModal
      show={show}
      showFooter={false}
      onHide={() => {
        setModalShow(false);
        setAmount(0);
        setMessage("");
        setPaymentStep(false);
      }}
      title={
        <>
          <div className={classes.title}>
            <span className="pe-2">
              <FaHandHoldingUsd />
            </span>
            WESPRZYJ ALEKSANDRĘ
          </div>
          <div className={classes.subtitle}>
            twórczynię bloga <span>Ale się zmieniłaś</span>
          </div>
        </>
      }
      body={
        paymentStep ? (
          <StripeContainer price={amount} message={message} />
        ) : (
          <>
            <QuickTip prices={prices} />
            <DonationForm
              setPaymentStep={setPaymentStep}
              amountState={[amount, setAmount]}
              setMessage={setMessage}
            />
          </>
        )
      }
      classModal={classes.modal}
      classTitle="w-100 d-flex flex-column justify-content-center align-items-center"
    />
  );
};

export default DonationModal;
