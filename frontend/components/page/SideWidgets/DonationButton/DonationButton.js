import BasicButton from "../../../basic/origin/ui/button/BasicButton";
import { FaHandHoldingUsd } from "react-icons/fa";
import classes from "./DonationButton.module.css";

const DonationButton = ({ setModalShow, text, type = "button", disabled }) => {
  return (
    <BasicButton
      type={type}
      onClick={setModalShow ? () => setModalShow(true) : null}
      classBtn={classes.btn}
      disabled={disabled}
    >
      <span className="me-2">
        <FaHandHoldingUsd className={classes.btnIcon} />
      </span>
      <span style={{ letterSpacing: "2px" }}>
        {text ? text : "WESPRZYJ AUTORA"}
      </span>{" "}
    </BasicButton>
  );
};

export default DonationButton;
