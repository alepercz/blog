import BasicCalendar from "../../basic/origin/features/calendar/BasicCalendar";
import classes from "./Calendar.module.css";

const Calendar = ({ dates }) => {
  return (
    <BasicCalendar
      todayButton="Wróć do dziś"
      datesToMark={dates}
      hoverDayStyle={{
        color: "white",
        backgroundColor: "var(--color-primary)",
      }}
      classCalendar={classes}
    />
  );
};

export default Calendar;
