import { useState } from "react";
import Calendar from "./Calendar";
import CategoryGroups from "./CategoryGroups";
import DonationButton from "./DonationButton/DonationButton";
import DonationModal from "./DonationModal/DonationModal";

const SideWidgets = ({ categories, dates, prices }) => {
  const [modalShow, setModalShow] = useState(false);

  return (
    <>
      <Calendar dates={dates} />
      <hr
        className="mt-0 mb-4 mx-3"
        style={{ color: "var(--color-primary)" }}
      />
      <CategoryGroups categories={categories} />
      <hr className="my-4 mx-3" style={{ color: "var(--color-primary)" }} />
      <DonationButton setModalShow={setModalShow} />
      <DonationModal
        show={modalShow}
        setModalShow={setModalShow}
        prices={prices}
      />
    </>
  );
};

export default SideWidgets;
