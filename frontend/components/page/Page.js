import PageHeader from "./PageHeader";
import Seo from "../basic/origin/layout/Seo";
import SideWidgets from "./SideWidgets/SideWidgets";
import { Container, Row, Col } from "react-bootstrap";

const Page = ({
  pageTitle = null,
  pageLink = null,
  postTitle = null,
  categories = null,
  dates = null,
  seo = null,
  article = false,
  prices = null,
  children,
}) => {
  return (
    <>
      <Seo seo={seo} article={article} />
      <PageHeader
        pageTitle={pageTitle}
        pageLink={pageLink}
        postTitle={postTitle}
      />
      <Container style={{ maxWidth: "1170px" }}>
        {categories && dates ? (
          <Row>
            <Col lg={8}>{children}</Col>
            <Col lg={4} className="d-none d-lg-grid sticky-lg-top h-100 py-3">
              <SideWidgets
                categories={categories}
                dates={dates}
                prices={prices}
              />
            </Col>
          </Row>
        ) : (
          <Row>{children}</Row>
        )}
      </Container>
    </>
  );
};

export default Page;
