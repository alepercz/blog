import Link from "next/link";
import Breadcrumb from "react-bootstrap/Breadcrumb";

const PageHeader = ({ pageTitle, pageLink, postTitle }) => {
  let page = [{ title: "Home", link: "/" }];

  if (pageTitle) {
    page.push({ title: pageTitle, link: pageLink ? pageLink : "#" });
  }
  if (postTitle) {
    page.push({ title: postTitle, link: "#" });
  }

  return (
    pageTitle && (
      <div
        className="text-white py-2 my-3"
        style={{ backgroundColor: "var(--color-primary)" }}
      >
        <hr className="m-0" />
        <Breadcrumb className="m-auto py-3 px-5" style={{ maxWidth: "1170px" }}>
          {page.map((item, i) => (
            <Breadcrumb.Item
              key={item.title}
              href={item.link}
              active={page.length - 1 === i}
            >
              {item.title}
            </Breadcrumb.Item>
          ))}
        </Breadcrumb>
        <hr className="m-0" />
      </div>
    )
  );
};

export default PageHeader;
