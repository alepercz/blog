import Link from "next/link";
import { useRouter } from "next/router";
import Image from "next/image";
import { getStrapiMedia } from "../../pages/api/media";
import classes from "./BlogHeader.module.css";

const BlogHeader = ({ blog }) => {
  const router = useRouter();

  return (
    <div className={classes.header}>
      <div onClick={() => router.push("/")}>
        <Image
          src={getStrapiMedia(blog.logo)}
          alt={blog.siteName}
          layout="fill"
          objectFit="cover"
        />
      </div>
      <Link href="/">
        <a>
          <h1>{blog.siteName}</h1>
        </a>
      </Link>
    </div>
  );
};

export default BlogHeader;
