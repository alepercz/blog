import { Container, Row, Col } from "react-bootstrap";

const Footer = ({ footer }) => {
  const { title, content, email, copyrights } = footer;
  return (
    <Container className="mw-100 mt-2">
      <Row
        className="justify-content-center"
        style={{ backgroundColor: "var(--color-base)" }}
      >
        <Col
          className="text-start text-white my-5"
          style={{ maxWidth: "700px" }}
        >
          <h5>{title}</h5>
          <hr />
          <p style={{ color: "var(--color-tetriary-dark)" }}>{content}</p>
          <span
            id="footerEmail"
            style={{ letterSpacing: "4px", fontSize: ".75rem" }}
          >
            {email}
          </span>
        </Col>
      </Row>
      <Row
        className="align-items-center"
        style={{
          backgroundColor: "var(--color-tetriary-light)",
          minHeight: "50px",
        }}
      >
        <Col className="text-center">{copyrights}</Col>
      </Row>
    </Container>
  );
};

export default Footer;
