import Footer from "./Footer";
import BlogHeader from "./BlogHeader";
import Navbar from "./Navbar";

const Layout = ({ blog, footer, menu, children }) => {
  return (
    <>
      <div
        style={{
          height: "22px",
          backgroundColor: "var(--color-tetriary-light)",
        }}
      />
      <BlogHeader blog={blog} />
      <Navbar navbarItems={menu} />
      <main>{children}</main>
      <Footer footer={footer} />
    </>
  );
};

export default Layout;
