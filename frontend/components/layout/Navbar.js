import { useRouter } from "next/router";
import { Navbar as NavBar, Nav, Container } from "react-bootstrap";
import BasicSearchBar from "../basic/origin/layout/navbar/searchBar/BasicSearchBar";
import classes from "./Navbar.module.css";

const Navbar = ({ navbarItems }) => {
  const router = useRouter();

  return (
    <NavBar collapseOnSelect expand="md" className={classes.menu}>
      <Container className={classes.container}>
        <NavBar.Toggle
          aria-controls="basic-navbar-nav"
          className="border-white"
        />
        <NavBar.Collapse id="basic-navbar-nav" className="my-3">
          <Nav className="mx-2 me-md-auto w-50 fw-light">
            {navbarItems.map((item) => (
              <Nav.Link
                key={item.title}
                href={item.pathname}
                active={
                  (item.slug && router.query.categorySlug === item.slug) ||
                  router.pathname === item.pathname
                }
                className="text-center text-nowrap w-25"
              >
                {item.title.toUpperCase()}
              </Nav.Link>
            ))}
          </Nav>
          <BasicSearchBar
            placeholder="Szukaj"
            classSearchInput={classes.searchInput}
            classSearchIcon={classes.searchIcon}
          />
        </NavBar.Collapse>
      </Container>
    </NavBar>
  );
};

export default Navbar;
