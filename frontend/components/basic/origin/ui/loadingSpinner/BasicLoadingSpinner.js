import ClipLoader from "react-spinners/ClipLoader";
import { css } from "@emotion/react";

const override = css`
  display: block;
  margin: 100px auto;
  border-color: var(--color-secondary);
`;

const BasicLoadingSpinner = ({ loading }) => {
  return (
    <ClipLoader color={"#ffffff"} loading={loading} css={override} size={100} />
  );
};

export default BasicLoadingSpinner;
