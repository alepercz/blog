import { Card } from "react-bootstrap";
import BasicPicture from "../../image/BasicPicture";
import { useWindowSize } from "../../../hook/useWindowSize";
import classes from "./BasicCategoryCard.module.css";

const BasicCategoryCard = ({
  image,
  title,
  onClick,
  classCard,
  styleCard,
  stylePicture,
  classCaption,
  styleCaption,
  classTitle,
  styleTitle,
}) => {
  const { width } = useWindowSize();
  return (
    <Card
      onClick={onClick}
      className={classCard ?? "my-2 mx-auto rounded-3"}
      style={
        styleCard ?? {
          minWidth: "200px",
          maxWidth: "360px",
          height: "240px",
          borderRadius: "4px",
          cursor: "pointer",
        }
      }
    >
      <BasicPicture
        type="image"
        picture={image}
        alt={title}
        classPicture="position-relative w-100 h-100 overflow-hidden"
        stylePicture={stylePicture ?? { borderRadius: "4px" }}
      />
      <Card.ImgOverlay
        className={classCaption ?? classes.caption}
        style={styleCaption ?? { top: `${width < 768 ? "50%" : "100%"}` }}
      >
        <Card.Title className={classTitle ?? classes.title} style={styleTitle}>
          {title}
        </Card.Title>
      </Card.ImgOverlay>
    </Card>
  );
};

export default BasicCategoryCard;
