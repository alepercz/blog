import { Card, Badge } from "react-bootstrap";

const BasicBadge = ({
  id,
  title,
  badge,
  onClick,
  pillShape = false,
  classCard,
  styleCard,
  classBody,
  styleBody,
  classTitle,
  styleTitle,
  classBadge,
  styleBadge,
}) => {
  return (
    <Card id={id} onClick={onClick} className={classCard} style={styleCard}>
      <Card.Body className={classBody} style={styleBody}>
        <Card.Title className={classTitle} style={styleTitle}>
          {title}
        </Card.Title>
        <Badge pill={pillShape} className={classBadge} style={styleBadge}>
          {badge}
        </Badge>
      </Card.Body>
    </Card>
  );
};

export default BasicBadge;
