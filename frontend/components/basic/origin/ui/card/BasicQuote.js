import { Card } from "react-bootstrap";

const BasicQuote = ({ header, quote, author, classQuote, styleQuote }) => {
  return (
    <Card className={classQuote} style={styleQuote}>
      <Card.Header>{header}</Card.Header>
      <Card.Body>
        <blockquote className="blockquote mb-0">
          <p>{quote}</p>
          <footer className="blockquote-footer">
            <cite>{author}</cite>
          </footer>
        </blockquote>
      </Card.Body>
    </Card>
  );
};

export default BasicQuote;
