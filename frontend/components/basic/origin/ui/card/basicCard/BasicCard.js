import { Card, Col } from "react-bootstrap";
import BasicPicture from "../../image/BasicPicture";
import CardElement from "./CardElement";

const BasicCard = ({
  vertical = true,
  imgLeft = true,
  src,
  alt,
  header,
  title,
  subtitle,
  text,
  footer,
  children,
  classCard,
  styleCard,
  classPicture,
  stylePicture,
  classBody,
  styleBody,
  classHeader,
  styleHeader,
  classTitle,
  styleTitle,
  classSubtitle,
  styleSubtitle,
  classText,
  styleText,
  classFooter,
  styleFooter,
  onClickCard = null,
  onClickHeader = null,
}) => {
  return (
    <Card
      className={classCard ?? `d-flex ${vertical ? "flex-column" : "flex-row"}`}
      style={
        styleCard ?? {
          borderRadius: "5px",
          margin: "50px auto",
          overflow: "hidden",
          cursor: "default",
        }
      }
      onClick={onClickCard}
    >
      {imgLeft && src && (
        <BasicPicture
          type="image"
          picture={src}
          alt={alt}
          classPicture={classPicture}
          stylePicture={stylePicture}
        />
      )}
      <Col>
        <Card.Body className={classBody} style={styleBody}>
          {header && (
            <CardElement
              type="header"
              content={header}
              classEl={classHeader ?? "text-center"}
              styleEl={styleHeader}
              onClick={onClickHeader}
            />
          )}
          {title && (
            <CardElement
              type="title"
              content={title}
              classEl={classTitle ?? "my-3"}
              styleEl={styleTitle}
            />
          )}
          {subtitle && (
            <CardElement
              type="subtitle"
              content={subtitle}
              classEl={classSubtitle}
              styleEl={styleSubtitle}
            />
          )}
          {text && (
            <CardElement
              type="text"
              content={text}
              classEl={classText}
              styleEl={styleText}
            />
          )}
          {children}
          {footer && (
            <CardElement
              type="footer"
              content={footer}
              classEl={classFooter}
              styleEl={styleFooter}
            />
          )}
        </Card.Body>
      </Col>
      {!imgLeft && (
        <BasicPicture
          type="image"
          picture={src}
          alt={alt}
          classPicture={classPicture}
          stylePicture={stylePicture}
        />
      )}
    </Card>
  );
};

export default BasicCard;
