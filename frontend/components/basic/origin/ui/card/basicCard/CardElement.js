import { Card } from "react-bootstrap";

const CardElement = ({ type = "text", content, onClick, classEl, styleEl }) => {
  switch (type) {
    case "header":
      return (
        <Card.Header className={classEl} style={styleEl} onClick={onClick}>
          {content}
        </Card.Header>
      );
    case "title":
      return (
        <Card.Title className={classEl} style={styleEl}>
          {content}
        </Card.Title>
      );
    case "subtitle":
      return (
        <Card.Subtitle className={classEl} style={styleEl}>
          {content}
        </Card.Subtitle>
      );
    case "text":
      return (
        <Card.Text className={classEl} style={styleEl}>
          {content}
        </Card.Text>
      );
    case "footer":
      return (
        <Card.Footer className={classEl} style={styleEl}>
          {content}
        </Card.Footer>
      );
    default:
      break;
  }
};

export default CardElement;
