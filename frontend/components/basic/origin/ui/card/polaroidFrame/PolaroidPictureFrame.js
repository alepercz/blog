import classes from "./PolaroidPictureFrame.module.css";

const PolaroidPictureFrame = ({
  src,
  alt,
  title = "Title",
  description = "Here goes your description",
  styleImg,
  classImg,
  styleFrame,
  classFrame,
}) => {
  return (
    <div className={classFrame ?? classes.frame} style={styleFrame}>
      <div className={classImg ?? classes.image} style={styleImg}>
        {src && (
          <img
            src={src}
            alt={alt}
            className="w-100 h-100"
            style={{
              objectFit: "cover",
            }}
          />
        )}
      </div>
      <div className={classes.description} style={{ left: "10%" }}>
        <h5>{title.toUpperCase()}</h5>
        <p>{description.toUpperCase()}</p>
      </div>
    </div>
  );
};

export default PolaroidPictureFrame;
