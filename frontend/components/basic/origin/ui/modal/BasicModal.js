import Modal from "react-bootstrap/Modal";
import BasicButton from "../button/BasicButton";

const BasicModal = ({
  showFooter = true,
  title,
  body,
  classModal,
  classTitle,
  ...props
}) => {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      contentClassName={classModal}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter" bsPrefix={classTitle}>
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>{body}</Modal.Body>
      {showFooter && (
        <Modal.Footer
          style={{ justifyContent: "space-between", marginLeft: "15px" }}
        >
          <BasicButton onClick={props.onHide}>Anuluj</BasicButton>
          <BasicButton onClick={props.onSubmit}>Dalej</BasicButton>
        </Modal.Footer>
      )}
    </Modal>
  );
};

export default BasicModal;
