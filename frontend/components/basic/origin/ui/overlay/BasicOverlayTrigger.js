import { OverlayTrigger, Tooltip } from "react-bootstrap";

// CHANGE classes in globals.css
// .tooltip-arrow::before {
//   border-bottom-color: aqua !important;  ---- if placement is "bottom"
//   border-top-color: aqua !important;  ---- if placement is "top" and so on...
// }
// .tooltip-inner {
//   background-color: aqua;
//   color: red;
// }

const BasicOverlayTrigger = ({
  text = "Simple tooltip",
  placement,
  children,
}) => {
  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      {text}
    </Tooltip>
  );

  return (
    <OverlayTrigger
      placement={placement}
      delay={{ show: 250, hide: 300 }}
      overlay={renderTooltip}
      trigger={["hover", "focus"]}
    >
      <span>{children}</span>
    </OverlayTrigger>
  );
};

export default BasicOverlayTrigger;
