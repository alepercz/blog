import Image from "next/image";
import { useRouter } from "next/router";
import { Carousel } from "react-bootstrap";
import { getStrapiMedia } from "../../../../../pages/api/media";
import classes from "./BasicSlider.module.css";

const BasicSlider = ({
  posts,
  classSlider,
  styleSlider,
  classSlide,
  styleSlide,
  classCaption,
  styleCaption,
  classTitle,
  styleTitle,
  classSubtitle,
  styleSubtitle,
}) => {
  const router = useRouter();

  return (
    <Carousel
      className={classSlider ?? "m-auto my-3 my-sm-4 my-md-5"}
      style={styleSlider ?? { maxWidth: "1170px", cursor: "pointer" }}
    >
      {posts.map((post) => (
        <Carousel.Item
          key={post.id}
          onClick={() => router.push(`/${post.category.slug}/${post.slug}`)}
          className={classSlide ?? classes.slide}
          style={styleSlide}
        >
          <Image
            src={getStrapiMedia(post.image)}
            alt={post.title}
            layout="fill"
            objectFit="cover"
          />
          <Carousel.Caption
            className={classCaption}
            style={styleCaption ?? { zIndex: "999" }}
          >
            <h5 className={classSubtitle ?? "rouded-3"} style={styleSubtitle}>
              {post.category.title.toUpperCase()}
            </h5>
            <h2 className={classTitle} style={styleTitle}>
              {post.title}
            </h2>
          </Carousel.Caption>
        </Carousel.Item>
      ))}
    </Carousel>
  );
};

export default BasicSlider;
