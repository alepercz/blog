import { Col } from "react-bootstrap";

const CircleImg = ({
  src,
  alt,
  imgWidth,
  imgHeight,
  styleImg,
  classImg,
  styleImgFrame,
  classImgFrame,
  styleImgCol,
  classImgCol,
  children,
}) => {
  return (
    <Col lg={4} className={classImgCol} style={styleImgCol}>
      <div
        className={
          classImgFrame ??
          "rounded-circle overflow-hidden p-0 mx-auto mt-4 mb-5 bg-light"
        }
        style={
          styleImgFrame ?? {
            minWidth: "250px",
            minHeight: "250px",
            maxWidth: "440px",
            maxHeight: "440px",
          }
        }
      >
        {src && (
          <img
            src={src}
            alt={alt ?? "Image"}
            width={imgWidth}
            height={imgHeight}
            style={
              styleImg ?? {
                objectFit: "cover",
              }
            }
            className={classImg ?? "w-100 h-100"}
          />
        )}
      </div>
      {children}
    </Col>
  );
};

export default CircleImg;
