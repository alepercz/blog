import Image from "next/image";

const BasicImage = ({ image, alt, classImage, styleImage }) => {
  return (
    <div
      className={classImage ?? "position-relative"}
      style={styleImage ?? { width: "100%", height: "250px" }}
    >
      <Image src={image} alt={alt ?? "image"} layout="fill" objectFit="cover" />
    </div>
  );
};

const BasicImg = ({ img, alt, classFrame, styleFrame, classImg, styleImg }) => {
  return (
    <div className={classFrame} style={styleFrame}>
      <div className={classImg} style={styleImg}>
        <img
          src={img}
          alt={alt ?? "img"}
          className="w-100 h-100"
          style={{
            objectFit: "cover",
          }}
        />
      </div>
    </div>
  );
};

// TYPE: "image" or "img" - image is from next/image
const BasicPicture = ({
  type = "img",
  picture,
  alt,
  classFrame,
  styleFrame,
  classPicture,
  stylePicture,
}) => {
  return type === "image" ? (
    <BasicImage
      image={picture}
      alt={alt}
      classImage={classPicture}
      styleImage={stylePicture}
    />
  ) : (
    <BasicImg
      img={picture}
      alt={alt}
      classImg={classPicture}
      styleImg={stylePicture}
      classFrame={classFrame}
      styleFrame={styleFrame}
    />
  );
};

export default BasicPicture;
