import { Button } from "react-bootstrap";
import classes from "./BasicButton.module.css";

const BasicButton = ({
  type,
  size,
  disabled,
  side = "center",
  margin = "my-3 me-4",
  href,
  onClick,
  classBtn,
  styleBtn,
  children,
}) => {
  return (
    <div className={`d-block text-${side} ${margin}`}>
      <Button
        type={type}
        size={size}
        href={href}
        onClick={onClick}
        className={classBtn ?? classes.btn}
        style={styleBtn}
        disabled={disabled}
      >
        {children}
      </Button>
    </div>
  );
};

export default BasicButton;
