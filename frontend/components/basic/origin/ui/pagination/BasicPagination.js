import { useRouter } from "next/router";
import { Pagination } from "react-bootstrap";
import classes from "./BasicPagination.module.css";

const BasicPagination = ({
  maxPageNum,
  pathname,
  classBtnActive,
  styleBtnActive,
  classPagination,
  stylePagination,
}) => {
  const router = useRouter();

  let items = [];
  for (let number = 1; number <= maxPageNum; number++) {
    items.push(
      <Pagination.Item
        key={number}
        className={classBtnActive ?? classes.btnActive}
        style={styleBtnActive}
        active={+router.query.page === number}
        onClick={() => router.push(`${pathname}?page=${number}`)}
      >
        {number}
      </Pagination.Item>
    );
  }

  const currentPage = +router.query.page;
  const prev = currentPage - 1;
  const next = currentPage + 1;

  return (
    maxPageNum > 1 && (
      <Pagination
        className={classPagination ?? classes.pagination}
        style={stylePagination}
      >
        {currentPage !== 1 && prev !== 1 && (
          <Pagination.First onClick={() => router.push(`${pathname}?page=1`)} />
        )}
        {currentPage > 1 && (
          <Pagination.Prev
            onClick={() => router.push(`${pathname}?page=${prev}`)}
          />
        )}
        {items}
        {currentPage < maxPageNum && (
          <Pagination.Next
            onClick={() => router.push(`${pathname}?page=${next}`)}
          />
        )}
        {currentPage !== maxPageNum && next !== maxPageNum && (
          <Pagination.Last
            onClick={() => router.push(`${pathname}?page=${maxPageNum}`)}
          />
        )}
      </Pagination>
    )
  );
};

export default BasicPagination;
