import { Alert } from "react-bootstrap";

const BasicAlert = ({ variant, title, message, show, onClose }) => {
  if (show) {
    return (
      <Alert variant={variant ?? "danger"} onClose={onClose} dismissible>
        <Alert.Heading>{title ?? "Oh snap! You got an error!"}</Alert.Heading>
        <p>
          {message ??
            "Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum."}
        </p>
      </Alert>
    );
  }
};

export default BasicAlert;
