import BasicPicture from "../ui/image/BasicPicture";

const ErrorPage = ({
  image,
  message,
  classErrorPage,
  styleErrorPage,
  stylePicture,
  classMessage,
  styleMessage,
}) => {
  return (
    <div
      className={classErrorPage ?? "position-relative my-5"}
      style={styleErrorPage}
    >
      <BasicPicture
        type="image"
        picture={image}
        alt={message}
        stylePicture={stylePicture ?? { width: "450px", height: "450px" }}
      />
      <h1
        className={
          classMessage ??
          "position-absolute top-50 start-50 translate-middle text-nowrap p-3 fw-bold"
        }
        style={
          styleMessage ?? {
            borderRadius: "100px",
          }
        }
      >
        {message}
      </h1>
    </div>
  );
};

export default ErrorPage;
