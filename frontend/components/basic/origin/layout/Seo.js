import Head from "next/head";
import { useContext } from "react";
import { GlobalContext } from "../../../../pages/_app";

const Seo = ({ seo, article }) => {
  const { defaultSeo, siteName } = useContext(GlobalContext);

  let seoWithDefaults;

  if (!seo) {
    seoWithDefaults = {
      ...defaultSeo,
    };
  } else {
    seoWithDefaults = {
      ...defaultSeo,
      ...seo,
    };
  }

  if (article) {
    seoWithDefaults = { ...seoWithDefaults, article: true };
  }

  const fullSeo = {
    ...seoWithDefaults,
    // Add title suffix
    metaTitle: `${seoWithDefaults.metaTitle} | ${siteName}`,
  };

  return (
    <Head>
      {fullSeo.metaTitle && (
        <>
          <title>{fullSeo.metaTitle}</title>
          <meta property="og:title" content={fullSeo.metaTitle} />
          <meta name="twitter:title" content={fullSeo.metaTitle} />
        </>
      )}
      {fullSeo.metaDescription && (
        <>
          <meta name="description" content={fullSeo.metaDescription} />
          <meta property="og:description" content={fullSeo.metaDescription} />
          <meta name="twitter:description" content={fullSeo.metaDescription} />
        </>
      )}
      {fullSeo.article && <meta property="og:type" content="article" />}
      <meta name="twitter:card" content="summary_large_image" />
    </Head>
  );
};

export default Seo;
