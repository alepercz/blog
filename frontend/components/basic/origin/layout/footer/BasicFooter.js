import { Container, Navbar } from "react-bootstrap";

const BasicFooter = ({ info }) => {
  return (
    <Navbar bg="light" style={{ height: "100px" }}>
      <Container style={{ backgroundColor: "" }}>
        <p style={{ margin: "auto" }}>
          2021 Copyrights{" "}
          <span
            style={{
              fontFamily: "var(--logo-font)",
              margin: "0 15px",
              fontSize: "1.5rem",
            }}
          >
            {info}
          </span>
          Developed by NEUE
        </p>
      </Container>
    </Navbar>
  );
};

export default BasicFooter;
