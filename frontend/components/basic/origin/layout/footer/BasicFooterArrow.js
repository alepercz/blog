import { HiOutlineArrowNarrowRight as Arrow } from "react-icons/hi";

const BasicFooterArrow = ({
  text,
  classText,
  styleText,
  classArrow,
  styleArrow,
}) => {
  return (
    <div
      className="d-flex align-items-center ms-auto"
      style={{ width: "fit-content" }}
    >
      <span className={classText} style={styleText}>
        {text}
      </span>
      <Arrow className={classArrow ?? "mx-2 fs-3"} style={styleArrow} />
    </div>
  );
};

export default BasicFooterArrow;
