const BasicSubheader = ({
  id,
  title,
  subtitle,
  classMainSubheader,
  styleMainSubheader,
  classSubheader,
  styleSubheader,
  children,
}) => {
  return (
    <>
      <h2
        id={id}
        className={
          classMainSubheader ??
          "position-relative mx-auto my-0 d-block text-center"
        }
        style={
          styleMainSubheader ?? { width: "fit-content", padding: "48px 0" }
        }
      >
        {title}
        {children}
      </h2>
      <h4 className={classSubheader} style={styleSubheader}>
        {subtitle}
      </h4>
    </>
  );
};

export default BasicSubheader;
