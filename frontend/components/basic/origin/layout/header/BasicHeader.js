const BasicHeader = ({ id, title, classHeader, styleHeader, children }) => {
  return (
    <h1
      id={id}
      className={
        classHeader ?? "position-relative mx-auto my-5 d-block text-center"
      }
      style={styleHeader ?? { width: "fit-content" }}
    >
      {title}
      {children}
    </h1>
  );
};

export default BasicHeader;
