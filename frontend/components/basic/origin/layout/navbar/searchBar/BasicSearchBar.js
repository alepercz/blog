import classes from "./BasicSearchBar.module.css";
import { GoSearch } from "react-icons/go";
import { Form, FormControl, Button } from "react-bootstrap";

const BasicSearchBar = ({
  placeholder,
  classSearchForm,
  styleSearchForm,
  classSearchInput,
  styleSearchInput,
  classSearchBtn,
  styleSearchBtn,
  classSearchIcon,
  styleSearchIcon,
}) => {
  return (
    <Form
      action="search"
      method="get"
      className={classSearchForm ?? "d-flex mt-2 mt-md-0"}
      style={styleSearchForm}
    >
      <FormControl
        type="search"
        placeholder={placeholder}
        className={classSearchInput ?? classes.input}
        style={styleSearchInput}
        aria-label="Search"
        name="results"
        autoComplete="off"
      />
      <Button
        type="submit"
        bsPrefix={classSearchBtn ?? classes.btn}
        style={styleSearchBtn}
      >
        <GoSearch
          className={classSearchIcon ?? classes.icon}
          style={styleSearchIcon}
        />
      </Button>
    </Form>
  );
};

export default BasicSearchBar;
