import { Container, Navbar, Nav } from "react-bootstrap";

const BasicNavbar = ({
  bg = "light",
  brand,
  menu,
  styleBrand,
  classBrand,
  styleNavItem,
  classNavItem,
}) => {
  return (
    <Navbar bg={bg} expand="xl">
      <Container>
        <Navbar.Brand href="/" style={styleBrand} className={classBrand}>
          {brand}
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            {menu.map((item) => (
              <Nav.Link
                key={item.name}
                href={item.link}
                className={classNavItem}
              >
                {item.name}
              </Nav.Link>
            ))}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default BasicNavbar;
