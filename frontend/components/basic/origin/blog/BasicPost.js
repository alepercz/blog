import { Container } from "react-bootstrap";
import BasicPicture from "../ui/image/BasicPicture";
import BasicPostHeader from "./postHeader/BasicPostHeader";
import BasicContent from "./postContent/BasicContent";
import { useWindowSize } from "../hook/useWindowSize";
import { option3 } from "../../../../utils/dateFormatOptions";

const BasicPost = ({
  image,
  alt,
  category,
  author,
  date,
  opinion,
  title,
  content,
  classHeader,
  styleHeader,
  classCategory,
  styleCategory,
  classBody,
  styleBody,
  classAuthor,
  styleAuthor,
  classOpinion,
  styleOpinion,
  classContent,
  isPost = true,
}) => {
  const { width } = useWindowSize();
  // isPost determinate is post type or article type of text

  return (
    <Container className={`my-3 mx-auto ${width < 768 ? "px-0" : "px-4"}`}>
      <BasicPicture
        type="image"
        picture={image}
        alt={alt}
        stylePicture={
          width < 768
            ? { height: "250px" }
            : {
                height: "450px",
                borderRadius: "10px 10px 0 0",
                overflow: "hidden",
              }
        }
      />
      {isPost && (
        <BasicPostHeader
          category={category.toUpperCase() ?? "Category"}
          author={author ?? "Author"}
          date={
            new Date(date).toLocaleDateString("pl-PL", option3) ?? new Date()
          }
          opinion={opinion ?? "Your opinion"}
          classHeader={classHeader}
          styleHeader={styleHeader}
          classCategory={classCategory}
          styleCategory={styleCategory}
          classBody={classBody}
          styleBody={styleBody}
          classAuthor={classAuthor}
          styleAuthor={styleAuthor}
          classOpinion={classOpinion}
          styleOpinion={styleOpinion}
        />
      )}
      <BasicContent
        title={title ?? "Title"}
        content={content ?? "Here goes your content."}
        classContent={classContent}
      />
    </Container>
  );
};

export default BasicPost;
