import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import classes from "./BasicContent.module.css";

const BasicContent = ({
  title,
  content,
  classTitle,
  styleTitle,
  classContent,
}) => {
  return (
    <div className={classContent ?? classes.content}>
      <h1 className={classTitle ?? "text-center my-5"} style={styleTitle}>
        {title}
      </h1>
      <ReactMarkdown
        rehypePlugins={[rehypeRaw]}
        transformImageUri={(uri) =>
          uri.startsWith("http")
            ? uri
            : `${process.env.REACT_IMAGE_BASE_URL}${uri}`
        }
      >
        {content}
      </ReactMarkdown>
    </div>
  );
};

export default BasicContent;
