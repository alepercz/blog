import { Row, Col } from "react-bootstrap";
import BasicCard from "../../ui/card/basicCard/BasicCard";

const BasicCommentListItem = ({
  picture,
  alt,
  nickname,
  date,
  comment,
  vertical = false,
  styleNickname,
  styleCard,
  classPicture,
  stylePicture,
  classTitle,
  classSubtitle,
  styleSubtitle,
  classText,
  styleText,
}) => {
  return (
    <Row>
      <Col>
        <BasicCard
          vertical={vertical}
          src={picture}
          alt={alt}
          title={
            <>
              <span style={styleNickname}>{nickname}</span> mówi:
            </>
          }
          subtitle={date}
          text={comment}
          styleCard={styleCard}
          classPicture={classPicture}
          stylePicture={stylePicture}
          classTitle={classTitle ?? ""}
          classSubtitle={classSubtitle}
          styleSubtitle={styleSubtitle}
          classText={classText}
          styleText={styleText}
        />
      </Col>
    </Row>
  );
};

export default BasicCommentListItem;
