import { Container } from "react-bootstrap";
import BasicSubheader from "../../layout/header/BasicSubheader";
import BasicForm from "./commentForm/BasicCommentForm";
import BasicCommentListItem from "./BasicCommentListItem";

const BasicCommentSection = ({
  // header
  id,
  header,
  subheader,
  classMainSubheader,
  styleMainSubheader,
  classSubheader,
  styleSubheader,
  // form
  postId,
  btnText,
  labelComment,
  labelNickname,
  recaptchaKey,
  setPostComments,
  classForm,
  styleForm,
  classLabelComment,
  styleLabelComment,
  classComment,
  styleComment,
  classLabelNickname,
  styleLabelNickname,
  classNickname,
  styleNickname,
  classBtn,
  styleBtn,
  // comments
  postComments,
  picture,
  alt,
  dateStyleOption,
  classCommentSection,
  classHrLine,
  styleHrLine,
  styleCommentNickname,
  styleCommentCard,
  classCommentTitle,
  classCommentSubtitle,
  styleCommentSubtitle,
  classCommentText,
  styleCommentText,
  classCommentPicture,
  styleCommentPicture,
}) => {
  return (
    <Container>
      <BasicSubheader
        title={header}
        subtitle={subheader}
        id={id}
        classMainSubheader={classMainSubheader}
        styleMainSubheader={styleMainSubheader}
        classSubheader={classSubheader}
        styleSubheader={styleSubheader}
      />
      <BasicForm
        postId={postId}
        btnText={btnText}
        labelComment={labelComment}
        labelNickname={labelNickname}
        recaptchaKey={recaptchaKey}
        setPostComments={setPostComments}
        classForm={classForm}
        styleForm={styleForm}
        classLabelComment={classLabelComment}
        styleLabelComment={styleLabelComment}
        classComment={classComment}
        styleComment={styleComment}
        classLabelNickname={classLabelNickname}
        styleLabelNickname={styleLabelNickname}
        classNickname={classNickname}
        styleNickname={styleNickname}
        classBtn={classBtn}
        styleBtn={styleBtn}
      />
      <div className={classCommentSection ?? "my-5"}>
        {postComments.length !== 0 && (
          <hr className={classHrLine ?? "my-4"} style={styleHrLine} />
        )}
        {postComments.map((comment) => (
          <BasicCommentListItem
            key={comment._id}
            picture={picture}
            alt={alt ?? "User"}
            nickname={comment.nickname}
            date={new Date(comment.created_at).toLocaleDateString(
              "pl-PL",
              dateStyleOption
            )}
            comment={comment.comment}
            styleNickname={styleCommentNickname}
            styleCard={styleCommentCard}
            classTitle={classCommentTitle}
            classSubtitle={classCommentSubtitle ?? "me-5 text-end"}
            styleSubtitle={styleCommentSubtitle}
            classText={classCommentText}
            styleText={styleCommentText}
            classPicture={classCommentPicture}
            stylePicture={styleCommentPicture}
          />
        ))}
      </div>
    </Container>
  );
};

export default BasicCommentSection;
