import $ from "jquery";
import { useEffect, useRef, useState } from "react";
import { Form, FloatingLabel, Row, Col } from "react-bootstrap";
import ReCAPTCHA from "react-google-recaptcha";
import BasicAlert from "../../../ui/alert/BasicAlert";
import BasicButton from "../../../ui/button/BasicButton";
import { useWindowSize } from "../../../hook/useWindowSize";
import {
  createComment,
  getPostComments,
} from "../../../../../../utils/commentsHelpers";
import classes from "./BasicCommentForm.module.css";

const BasicForm = ({
  postId,
  btnText,
  labelComment,
  labelNickname,
  recaptchaKey,
  setPostComments,
  classForm,
  styleForm,
  classLabelComment,
  styleLabelComment,
  classComment,
  styleComment,
  classLabelNickname,
  styleLabelNickname,
  classNickname,
  styleNickname,
  classBtn,
  styleBtn,
}) => {
  const [form, setForm] = useState({ postID: postId });
  const [error, setError] = useState("");
  const { width } = useWindowSize();
  const reRef = useRef();

  useEffect(() => {
    setForm({ postID: postId });
    setError("");
  }, [postId]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const token = await reRef.current.executeAsync();
    const res = await createComment({ ...form, token: token });
    if (res.ok) {
      const comments = await getPostComments(postId);
      setPostComments(comments);
    } else {
      setError(
        "Wystąpił błąd przy dodawaniu komentarza. Spróbuj ponownie później."
      );
    }
    // reset comment form
    setForm({ postID: postId });
    $("#commentForm")[0].reset();
    reRef.current.reset();
  };

  const handleInputChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  return (
    <>
      {error && (
        <BasicAlert
          title="Error in da house!"
          message={error}
          show={error}
          onClose={() => setError("")}
        />
      )}
      <Form
        id="commentForm"
        onSubmit={handleSubmit}
        className={classForm ?? "mt-3"}
        style={styleForm}
      >
        <ReCAPTCHA ref={reRef} sitekey={recaptchaKey} size="invisible" />
        <FloatingLabel
          controlId="comment"
          label={labelComment}
          className={classLabelComment ?? classes.labelComment}
          style={styleLabelComment}
        >
          <Form.Control
            as="textarea"
            placeholder=" "
            className={classComment ?? classes.comment}
            style={styleComment ?? { height: "100px" }}
            name="comment"
            onChange={handleInputChange}
            required
            autoComplete="off"
          />
        </FloatingLabel>
        <Row
          className={`flex-${
            width < 576 ? "column" : "row"
          } align-items-center mt-2`}
        >
          <Col>
            <FloatingLabel
              controlId="nickname"
              label={labelNickname}
              className={classLabelNickname ?? classes.labelNickname}
              style={styleLabelNickname}
            >
              <Form.Control
                type="text"
                name="nickname"
                placeholder=" "
                onChange={handleInputChange}
                required
                autoComplete="off"
                className={classNickname ?? classes.nickname}
                style={styleNickname ?? { height: "50px" }}
              />
            </FloatingLabel>
          </Col>
          <Col>
            <BasicButton
              type="submit"
              side="end"
              classBtn={classBtn ?? classes.btn}
              styleBtn={styleBtn}
            >
              {btnText}
            </BasicButton>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default BasicForm;
