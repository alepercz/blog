import { Container, Row, Col } from "react-bootstrap";
import BasicCard from "../../ui/card/basicCard/BasicCard";
import BasicSubheader from "../../layout/header/BasicSubheader";

const PrevNextPage = ({
  header,

  prevHeader,
  prevImage,
  prevAlt,
  prevTitle,
  prevLink,

  nextHeader,
  nextImage,
  nextAlt,
  nextTitle,
  nextLink,

  vertical,
  imgLeftPrev,
  imgLeftNext,
  classContainer,
  styleContainer,
  classMainSubheader,
  styleMainSubheader,
  classSubheader,
  styleSubheader,
  classColPrev,
  classColNext,
  classCard,
  styleCard,
  classBodyPrev,
  classBodyNext,
  styleBody,
  classPicture,
  stylePicturePrev,
  stylePictureNext,
  classHeader,
  styleHeader,
  classTitle,
  styleTitle,
}) => {
  return (
    <Container className={classContainer} style={styleContainer}>
      {header && (
        <BasicSubheader
          title={header}
          classMainSubheader={classMainSubheader}
          styleMainSubheader={styleMainSubheader}
          classSubheader={classSubheader}
          styleSubheader={styleSubheader}
        />
      )}
      <Row>
        <Col sm={12} md={6} className={classColPrev ?? "text-center m-auto"}>
          {prevTitle && (
            <BasicCard
              vertical={vertical}
              imgLeft={imgLeftPrev}
              header={prevHeader}
              title={prevTitle}
              src={prevImage}
              alt={prevAlt}
              onClickCard={prevLink}
              classCard={classCard}
              styleCard={styleCard}
              classBody={classBodyPrev}
              styleBody={styleBody}
              classPicture={classPicture}
              stylePicture={stylePicturePrev}
              classHeader={classHeader}
              styleHeader={styleHeader}
              classTitle={classTitle}
              styleTitle={styleTitle}
            />
          )}
        </Col>
        <Col sm={12} md={6} className={classColNext ?? "text-center m-auto"}>
          {nextTitle && (
            <BasicCard
              vertical={vertical}
              imgLeft={imgLeftNext}
              header={nextHeader}
              title={nextTitle}
              src={nextImage}
              alt={nextAlt}
              onClickCard={nextLink}
              classCard={classCard}
              styleCard={styleCard}
              classBody={classBodyNext}
              styleBody={styleBody}
              classPicture={classPicture}
              stylePicture={stylePictureNext}
              classHeader={classHeader}
              styleHeader={styleHeader}
              classTitle={classTitle}
              styleTitle={styleTitle}
            />
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default PrevNextPage;
