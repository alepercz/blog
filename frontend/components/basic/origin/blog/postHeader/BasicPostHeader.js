import Link from "next/link";
import { Card } from "react-bootstrap";
import { BsVectorPen, BsChatRightText } from "react-icons/bs";

const BasicPostHeader = ({
  category,
  author,
  date,
  opinion,
  classHeader,
  styleHeader,
  classCategory,
  styleCategory,
  classBody,
  styleBody,
  classAuthor,
  styleAuthor,
  styleDate,
  classOpinion,
  styleOpinion,
}) => {
  return (
    <Card
      className={classHeader ?? "border-0 mt-1"}
      style={styleHeader ?? { cursor: "default" }}
    >
      <Card.Header
        className={classCategory ?? "text-center py-1 bg-transparent"}
        style={styleCategory ?? { cursor: "pointer", color: "grey" }}
      >
        {category}
      </Card.Header>
      <Card.Body
        className={classBody ?? "d-flex justify-content-between"}
        style={styleBody ?? { borderBottom: "1px solid grey" }}
      >
        <span
          className={classAuthor ?? "d-flex align-items-center"}
          style={styleAuthor}
        >
          <BsVectorPen className="me-2" />
          {author}
        </span>
        <span className={styleDate}>{date}</span>
        <span
          className={classOpinion ?? "d-flex align-items-center"}
          style={styleOpinion}
        >
          <Link href="#comments">
            <a>
              <BsChatRightText className="me-2" />
              {opinion}
            </a>
          </Link>
        </span>
      </Card.Body>
    </Card>
  );
};

export default BasicPostHeader;
