import { useState } from "react";
import DayPicker from "react-day-picker";
import {
  MONTHS,
  WEEKDAYS_LONG,
  WEEKDAYS_SHORT,
  FIRST_DAY_OF_WEEK,
  LABELS,
} from "../../../../../utils/calendarPolishLanguage";
import classes from "./BasicCalendar.module.css";

const BasicCalendar = ({
  todayButton,
  datesToMark,
  hoverDayStyle,
  classCalendar,
}) => {
  const [selectedDay, setSelectedDay] = useState();
  const handleDayMouseEnter = (day) => {
    setSelectedDay(day);
  };

  const modifiersStyles = {
    onMouseEnter: hoverDayStyle,
  };

  return (
    <DayPicker
      fixedWeeks
      showWeekNumbers
      todayButton={todayButton}
      modifiers={{
        today: new Date(),
        onMouseEnter: selectedDay,
      }}
      onDayMouseEnter={handleDayMouseEnter}
      modifiersStyles={modifiersStyles}
      selectedDays={datesToMark}
      months={MONTHS}
      weekdaysLong={WEEKDAYS_LONG}
      weekdaysShort={WEEKDAYS_SHORT}
      firstDayOfWeek={FIRST_DAY_OF_WEEK}
      labels={LABELS}
      classNames={classCalendar ?? classes}
    />
  );
};

export default BasicCalendar;
