import { useState, useEffect } from "react";
import TimeBlock from "./TimeBlock";

// END DATE FORMAT "06/18/2022 16:00:00"
const CountDownTimer = ({
  endDate,
  classTime,
  styleTime,
  classDesc,
  styleDesc,
  classTimeBlock,
  styleTimeBlock,
  classCountDown,
  styleCountDown,
}) => {
  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);

  useEffect(() => {
    const dayInMiliSec = 24 * 60 * 60 * 1000;
    const target = new Date(endDate);

    const interval = setInterval(() => {
      const now = new Date();
      const difference = target.getTime() - now.getTime();

      const d = Math.floor(difference / dayInMiliSec);
      const h = Math.floor((difference % dayInMiliSec) / (60 * 60 * 1000));
      const m = Math.floor((difference % (60 * 60 * 1000)) / (60 * 1000));
      const s = Math.floor((difference % (60 * 1000)) / 1000);

      setDays(d);
      setHours(h);
      setMinutes(m);
      setSeconds(s);
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <div className={classCountDown ?? "d-flex mx-auto"} style={styleCountDown}>
      <TimeBlock
        time={days}
        desc="DNI"
        classTime={classTime}
        styleTime={styleTime}
        classDesc={classDesc}
        styleDesc={styleDesc}
        classTimeBlock={classTimeBlock}
        styleTimeBlock={styleTimeBlock}
      />
      <TimeBlock
        time={hours}
        desc="GODZINY"
        classTime={classTime}
        styleTime={styleTime}
        classDesc={classDesc}
        styleDesc={styleDesc}
        classTimeBlock={classTimeBlock}
        styleTimeBlock={styleTimeBlock}
      />
      <TimeBlock
        time={minutes}
        desc="MINUTY"
        classTime={classTime}
        styleTime={styleTime}
        classDesc={classDesc}
        styleDesc={styleDesc}
        classTimeBlock={classTimeBlock}
        styleTimeBlock={styleTimeBlock}
      />
      <TimeBlock
        time={seconds}
        desc="SEKUNDY"
        classTime={classTime}
        styleTime={styleTime}
        classDesc={classDesc}
        styleDesc={styleDesc}
        classTimeBlock={classTimeBlock}
        styleTimeBlock={styleTimeBlock}
      />
    </div>
  );
};

export default CountDownTimer;
