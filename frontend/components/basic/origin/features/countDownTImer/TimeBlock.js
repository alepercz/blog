const TimeBlock = ({
  time,
  desc,
  classTime,
  styleTime,
  classDesc,
  styleDesc,
  classTimeBlock,
  styleTimeBlock,
}) => {
  return (
    <div
      className={
        classTimeBlock ??
        "d-flex flex-column justify-content-center align-items-center mx-3"
      }
      style={styleTimeBlock}
    >
      <div className={classTime ?? "fs-2"} style={styleTime}>
        {time}
      </div>
      <div className={classDesc} style={styleDesc}>
        {desc}
      </div>
    </div>
  );
};

export default TimeBlock;
