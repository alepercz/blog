import Link from "next/link";
import BasicOverlayTrigger from "../../ui/overlay/BasicOverlayTrigger";
import {
  BsEmojiSmile,
  BsEmojiLaughing,
  BsEmojiHeartEyes,
  BsEmojiFrown,
  BsEmojiNeutral,
  BsEmojiDizzy,
  BsEmojiAngry,
} from "react-icons/bs";

const BasicEmojiItem = ({
  reactionOption,
  reactionCount,
  summary = false,
  link,
  onClick,
  styleEmoji,
  classNumber,
  styleNumber,
}) => {
  const type = reactionOption;
  let emoji;
  switch (type) {
    case "hearts":
      emoji = <BsEmojiHeartEyes style={styleEmoji} onClick={onClick} />;
      break;
    case "haha":
      emoji = <BsEmojiLaughing style={styleEmoji} onClick={onClick} />;
      break;
    case "smile":
      emoji = <BsEmojiSmile style={styleEmoji} onClick={onClick} />;
      break;
    case "wow":
      emoji = <BsEmojiDizzy style={styleEmoji} onClick={onClick} />;
      break;
    case "neutral":
      emoji = <BsEmojiNeutral style={styleEmoji} onClick={onClick} />;
      break;
    case "sad":
      emoji = <BsEmojiFrown style={styleEmoji} onClick={onClick} />;
      break;
    case "angry":
      emoji = <BsEmojiAngry style={styleEmoji} onClick={onClick} />;
      break;
    default:
      break;
  }

  let description;
  switch (type) {
    case "hearts":
      description = "Uwielbiam!";
      break;
    case "haha":
      description = "Haha!";
      break;
    case "smile":
      description = "Podoba mi się";
      break;
    case "wow":
      description = "Wow!";
      break;
    case "neutral":
      description = "Obojętne mi";
      break;
    case "sad":
      description = "Smutne";
      break;
    case "angry":
      description = "Czuję gniew";
      break;
    default:
      break;
  }

  return (
    <>
      <BasicOverlayTrigger placement="top" text={description}>
        <div
          className="d-flex flex-column align-items-center"
          style={{ cursor: "pointer" }}
        >
          <Link href={link}>
            <a style={{ all: "unset" }}>{emoji}</a>
          </Link>
          {summary && (
            <span className={classNumber ?? "py-3 fs-3"} style={styleNumber}>
              {reactionCount}
            </span>
          )}
        </div>
      </BasicOverlayTrigger>
    </>
  );
};
export default BasicEmojiItem;
