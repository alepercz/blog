import { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import BasicSubheader from "../../layout/header/BasicSubheader";
import BasicEmojiItem from "./BasicEmojiItem";
import {
  createReaction,
  getPostReactions,
  preparePostReactionObj,
  updateReaction,
} from "../../../../../utils/emojiReactionsHelpers";
import BasicAlert from "../../ui/alert/BasicAlert";

const reactionList = [
  { option: "hearts" },
  { option: "haha" },
  { option: "smile" },
  { option: "wow" },
  { option: "neutral" },
  { option: "sad" },
  { option: "angry" },
];

const BasicEmojiReactions = ({
  postId,
  postSlug,
  postCategorySlug,
  reactionTitle = "What do You think about this article?",
  classContainer,
  styleContainer,
  styleEmoji,
  styleEmojiMatch,
  styleNumber,
  styleNumberMatch,
}) => {
  const [reactions, setReactions] = useState("");
  const [currentUserReaction, setCurrentUserReaction] = useState("");
  const [error, setError] = useState("");

  useEffect(() => {
    // window.localStorage.clear();
    async function getReaction() {
      const [reactions] = await getPostReactions(postId);
      if (reactions) {
        setReactions(reactions);
      }
    }
    getReaction();
    if (window.localStorage.getItem(postId)) {
      const userReaction = window.localStorage.getItem(postId);
      setCurrentUserReaction(userReaction);
    } else {
      setCurrentUserReaction("");
    }
  }, [postId]);

  const reactionHandler = async (reaction) => {
    let res;
    const postReactionObj = preparePostReactionObj(
      postId,
      postSlug,
      postCategorySlug,
      reactionList,
      reactions,
      reaction.option,
      currentUserReaction
    );

    if (reactions && currentUserReaction !== reaction.option) {
      res = await updateReaction(postReactionObj);
    } else if (!reactions) {
      res = await createReaction(postReactionObj);
    }

    if (res && res.ok) {
      if (currentUserReaction) {
        window.localStorage.removeItem(postId);
      }
      window.localStorage.setItem(postId, reaction.option);
      setCurrentUserReaction(reaction.option);
    } else if (res && !res.ok) {
      setError(
        "Wystąpił błąd przy dodawaniu reakcji. Spróbuj ponownie później."
      );
    }

    if (res?.status === 201) {
      const [reactions] = await getPostReactions(postId);
      setReactions(reactions);
    }
  };

  return (
    <Container className={classContainer} style={styleContainer}>
      <BasicSubheader
        title={reactionTitle}
        styleMainSubheader={{ fontWeight: "200", padding: "24px 0" }}
      />
      {error && (
        <BasicAlert
          title="Error in da house!"
          message={error}
          show={error}
          onClose={() => setError("")}
        />
      )}
      <Row
        style={
          currentUserReaction ? { marginBottom: "0" } : { marginBottom: "74px" }
        }
      >
        {reactions.reactionsCount
          ? reactions.reactionsCount.map((reaction) => (
              <Col
                xs={3}
                sm={1}
                md="auto"
                key={reaction.option}
                className="mx-auto"
              >
                <BasicEmojiItem
                  reactionOption={reaction.option}
                  reactionCount={reaction.count}
                  summary={Boolean(currentUserReaction)}
                  link="#comments"
                  onClick={() => reactionHandler(reaction)}
                  styleEmoji={
                    reaction.option === currentUserReaction
                      ? styleEmojiMatch
                      : styleEmoji
                  }
                  styleNumber={
                    reaction.option === currentUserReaction
                      ? styleNumberMatch
                      : styleNumber
                  }
                />
              </Col>
            ))
          : reactionList.map((reaction) => (
              <Col
                xs={3}
                sm={1}
                md="auto"
                key={reaction.option}
                className="mx-auto"
              >
                <BasicEmojiItem
                  key={reaction.option}
                  reactionOption={reaction.option}
                  link="#comment"
                  onClick={() => reactionHandler(reaction)}
                  styleEmoji={
                    reaction.option === currentUserReaction
                      ? styleEmojiMatch
                      : styleEmoji
                  }
                />
              </Col>
            ))}
      </Row>
    </Container>
  );
};

export default BasicEmojiReactions;
