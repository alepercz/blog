import { useEffect, useRef } from "react";
import { Loader } from "@googlemaps/js-api-loader";

// MARKERS ARRAY STRUCTURE
//  [
//      [{lat: 123.12, lng: 123.44}, "A"],
//      [{lat: 456.33, lng: 456.55}, "B"]]
//  ]

const centerView = (firstPoint, secPoint) => {
  const centerLat = (firstPoint.lat + secPoint.lat) / 2;
  const centerLng = (firstPoint.lng + secPoint.lng) / 2;

  return { lat: centerLat, lng: centerLng };
};

const GoogleMap = ({ apiKey, markers, width, height }) => {
  const googlemap = useRef(null);

  useEffect(() => {
    const loader = new Loader({
      apiKey: apiKey,
      version: "weekly",
    });

    let map;
    loader.load().then(() => {
      const google = window.google;
      map = new google.maps.Map(googlemap.current, {
        center: centerView(markers[0][0], markers[markers.length - 1][0]),
        zoom: 11,
      });

      const infoWindow = new google.maps.InfoWindow();

      markers.forEach(([position, title]) => {
        const marker = new google.maps.Marker({
          position,
          map,
          title: `${title}`,
          label: `${title[0]}`,
          optimized: false,
        });
        marker.addListener("click", () => {
          infoWindow.close();
          infoWindow.setContent(marker.getTitle());
          infoWindow.open(marker.getMap(), marker);
        });
      });
    });
  }, []);

  return (
    <div
      id="map"
      ref={googlemap}
      style={{ width: `${width}`, height: `${height}` }}
    />
  );
};

export default GoogleMap;
