import { useState, useEffect } from "react";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import PaymentForm from "./PaymentForm";
import BasicLoadingSpinner from "../../ui/loadingSpinner/BasicLoadingSpinner";
import axios from "axios";

const stripePromise = loadStripe(process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY);

const StripeContainer = ({ price, message }) => {
  const [clientSecret, setClientSecret] = useState("");
  const amount = price * 100;

  useEffect(() => {
    const getClientSecret = async () => {
      const { data: clientSecret } = await axios.post("/api/paymentIntents", {
        amount,
        message,
      });
      setClientSecret(clientSecret);
    };

    getClientSecret();
  }, []);

  return clientSecret ? (
    <Elements options={{ clientSecret }} stripe={stripePromise}>
      <PaymentForm isMessage={message.trim() ? 1 : 0} />
    </Elements>
  ) : (
    <BasicLoadingSpinner loading={!clientSecret} />
  );
};

export default StripeContainer;
