import { useState, useEffect } from "react";
import {
  PaymentElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";

const PaymentForm = ({ isMessage }) => {
  const stripe = useStripe();
  const elements = useElements();

  const [status, setStatus] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!stripe) {
      return;
    }
    const clientSecret = new URLSearchParams(window.location.search).get(
      "payment_intent_client_secret"
    );
    if (!clientSecret) {
      return;
    }

    stripe.retrievePaymentIntent(clientSecret).then(({ paymentIntent }) => {
      switch (paymentIntent.status) {
        case "succeeded":
          setStatus("Płatność przebiegła pomyślnie!");
          break;
        case "processing":
          setStatus("Twoja płatność jest przetwarzana.");
          break;
        case "requires_payment_method":
          setStatus("Twoja płatność nie powiodła się, spróbuj ponownie.");
          break;
        default:
          setStatus("Coś poszło nie tak...");
          break;
      }
    });
  }, [stripe]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!stripe || !elements) {
      return;
    }

    setIsLoading(true);

    const { error } = await stripe.confirmPayment({
      elements,
      confirmParams: {
        return_url: `http://localhost:3000/donation?message=${isMessage}`,
      },
    });

    if (error.type === "card_error" || error.type === "validation_error") {
      setStatus(error.message);
    } else {
      setStatus("Wystąpił nieoczekiwany błąd.");
    }

    setIsLoading(false);
  };

  return (
    <form id="payment-form" onSubmit={handleSubmit}>
      <PaymentElement id="payment-element" />
      <button
        id="payment-button"
        disabled={isLoading || !stripe || !elements}
        type="submit"
      >
        {isLoading ? <div className="spinner" id="spinner"></div> : "Zapłać"}
      </button>
      {status && <div id="payment-message">{status}</div>}
    </form>
  );
};

export default PaymentForm;
