import Link from "next/link";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import {
  AiOutlineFacebook,
  AiOutlineInstagram,
  AiOutlineMail,
} from "react-icons/ai";

const Media = ({ media, styleIcon, classIcon, children }) => {
  return (
    <Link href={media}>
      <a
        target="_blank"
        className={classIcon}
        style={styleIcon ?? { width: "35px" }}
      >
        {children}
      </a>
    </Link>
  );
};

const SocialMedia = ({
  facebook,
  instagram,
  email,
  classIcon,
  styleIcon,
  classSocialMedia,
  styleSocialMedia,
}) => {
  return (
    <div
      className={classSocialMedia ?? "d-flex justify-content-evenly m-auto"}
      style={styleSocialMedia ?? { width: "200px" }}
    >
      <Media media={facebook} classIcon={classIcon} styleIcon={styleIcon}>
        <AiOutlineFacebook className="w-100 h-100" />
      </Media>
      <Media media={instagram} classIcon={classIcon} styleIcon={styleIcon}>
        <AiOutlineInstagram className="w-100 h-100" />
      </Media>
      <OverlayTrigger
        placement="bottom"
        overlay={
          <Tooltip id="button-tooltip-2" className="text-nowrap">
            {email}
          </Tooltip>
        }
      >
        {({ ref, ...triggerHandler }) => (
          <div
            ref={ref}
            {...triggerHandler}
            className={classIcon}
            style={styleIcon ?? { width: "35px" }}
          >
            <AiOutlineMail className="w-100 h-100" />
          </div>
        )}
      </OverlayTrigger>
    </div>
  );
};

export default SocialMedia;
