const MultipleLineContent = ({ classContent, styleContent, children }) => {
  return (
    <p
      className={classContent ?? "text-center lh-lg"}
      style={
        styleContent ?? {
          whiteSpace: "pre-line",
          fontSize: "calc(1rem + .2vw)",
        }
      }
    >
      {children}
    </p>
  );
};

export default MultipleLineContent;
