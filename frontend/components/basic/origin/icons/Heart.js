import classes from "./Heart.module.css";

const Heart = ({ color, classHeart }) => {
  return (
    <div className={classHeart ?? classes.heart_1}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        // width="103.52"
        // height="90.625"
        viewBox="0 0 103.52 90.625"
      >
        <path
          id="Path_603"
          data-name="Path 603"
          d="M806.158,1697.775s-24.634-29.73-46.276-9.9,42.677,82.579,42.677,82.579,72.539-52.5,51.333-78.338S806.158,1697.775,806.158,1697.775Z"
          transform="translate(-754.847 -1680.48)"
          fill="none"
          stroke={color}
          strokeWidth="1"
        />
      </svg>
    </div>
  );
};

export default Heart;
