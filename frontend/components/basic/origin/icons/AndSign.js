import classes from "./AndSign.module.css";

const AndSign = ({ color, classAndSign, styleAndSign }) => {
  return (
    <div className={classAndSign ?? classes.andSign} style={styleAndSign}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        // width="265.282"
        // height="334.961"
        viewBox="0 0 265.282 334.961"
      >
        <path
          id="Path_583"
          data-name="Path 583"
          d="M324.365,653.659S-17.047,322.846,179.313,325.285c103.874,1.29,47.274,65.618-18.734,138.377C102.134,528.086,36.2,599.233,69.684,638.239c71.1,82.815,241.829-118.719,241.829-118.719"
          transform="translate(-59.779 -324.272)"
          fill="none"
          stroke={color}
          strokeWidth="1"
        />
      </svg>
    </div>
  );
};

export default AndSign;
