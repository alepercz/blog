import BasicPagination from "../basic/origin/ui/pagination/BasicPagination";
import classes from "./Pagination.module.css";

const Pagination = ({ maxPageNum, pathname }) => {
  return (
    <BasicPagination
      maxPageNum={maxPageNum}
      pathname={pathname}
      classPagination={classes.pagination}
      classBtnActive={classes.btnActive}
    />
  );
};

export default Pagination;
