import BasicButton from "../basic/origin/ui/button/BasicButton";
import classes from "./Button.module.css";

const Button = ({ type, link = "#", onClick = null, children }) => {
  return (
    <BasicButton
      type={type ?? "button"}
      size="lg"
      margin="my-5"
      href={link}
      onClick={onClick}
      classBtn={classes.btn}
      styleBtn={{
        backgroundColor: "var(--color-primary)",
        borderColor: "var(--color-tetriary-light)",
        borderRadius: "100px",
      }}
    >
      {children}
    </BasicButton>
  );
};

export default Button;
