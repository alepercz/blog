export const preparePostReactionObj = (
  postId,
  postSlug,
  postCategorySlug,
  reactionList,
  reactions,
  currentUserReaction,
  previousUserReaction
) => {
  let postReactions;
  let reactionsCount;

  if (reactions) {
    // reaction for this post exist in db
    postReactions = { ...reactions };
    reactionsCount = postReactions.reactionsCount;
  } else {
    // reaction for this post does not exist yet in db
    postReactions = {
      postID: postId,
      titleSlug: postSlug,
      categorySlug: postCategorySlug,
    };
    reactionsCount = reactionList.map((reaction) => ({
      ...reaction,
      count: 0,
    }));
  }

  const objIndex = reactionsCount.findIndex(
    (reaction) => reaction.option === currentUserReaction
  );
  reactionsCount[objIndex].count++;

  if (previousUserReaction) {
    // user change his answer and we have to remove his previous answer
    const prevObjIndex = reactionsCount.findIndex(
      (reaction) => reaction.option === previousUserReaction
    );
    reactionsCount[prevObjIndex].count--;
  }

  postReactions.reactionsCount = reactionsCount;

  return postReactions;
};

export const createReaction = async (postReactions) => {
  try {
    const res = await fetch("http://localhost:3000/api/reactions", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(postReactions),
    });
    return res;
  } catch (error) {
    return error;
  }
};

export const updateReaction = async (postReactions) => {
  try {
    const res = await fetch(
      `http://localhost:3000/api/reactions/${postReactions.postID}`,
      {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(postReactions),
      }
    );
    return res;
  } catch (error) {
    return error;
  }
};

// helper to GET POST'S REACTIONS for the postID from MongoDB
// getPostReactions(post.id);
export async function getPostReactions(postID) {
  const res = await fetch("http://localhost:3000/api/reactions");
  const { data } = await res.json();
  const postReactions = data.filter((reaction) => reaction.postID == postID);
  return postReactions;
}

// helper to GET all POST'S REACTIONS from MongoDB
// getAllPostReactions();
export async function getAllPostReactions() {
  const res = await fetch("http://localhost:3000/api/reactions");
  const { data } = await res.json();
  return data;
}
