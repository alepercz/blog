export const createComment = async (formData) => {
  try {
    const res = await fetch("http://localhost:3000/api/comments", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nickname: formData.nickname,
        comment: formData.comment,
        token: formData.token,
        postID: formData.postID,
      }),
    });
    return res;
  } catch (error) {
    return error;
  }
};

export async function getComments() {
  const res = await fetch("http://localhost:3000/api/comments");
  const { data } = await res.json();
  return data;
}

export const getPostComments = async (postID) => {
  const res = await fetch("http://localhost:3000/api/comments");
  const { data } = await res.json();
  const postComments = data.filter((comment) => comment.postID == postID);
  const sortedComments = postComments.sort((a, b) => {
    return new Date(b.created_at) - new Date(a.created_at);
  });
  return sortedComments;
};
