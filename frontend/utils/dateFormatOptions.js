export const option1 = {
  weekday: "short",
  day: "2-digit",
  month: "long",
  year: "numeric",
};

export const option2 = {
  day: "2-digit",
  month: "long",
  year: "numeric",
};

export const option3 = {
  weekday: "long",
  day: "2-digit",
  month: "long",
  year: "numeric",
};

export const option4 = {
  day: "2-digit",
  month: "short",
  year: "numeric",
};
