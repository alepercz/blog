export const MONTHS = [
  "Styczeń",
  "Luty",
  "Marzec",
  "Kwiecień",
  "Maj",
  "Czerwiec",
  "Lipiec",
  "Sierpień",
  "Wrzesień",
  "Październik",
  "Listopad",
  "Grudzień",
];
export const WEEKDAYS_LONG = [
  "Niedziela",
  "Poniedziałek",
  "Wtorek",
  "Środa",
  "Czwartek",
  "Piątek",
  "Sobota",
];
export const WEEKDAYS_SHORT = ["Nd", "Pn", "Wt", "Śr", "Czw", "Pt", "Sb"];
export const FIRST_DAY_OF_WEEK = 1;
export const LABELS = {
  nextMonth: "Następny miesiąc",
  previousMonth: "Poprzedni miesiąc",
};
