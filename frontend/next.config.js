module.exports = {
  reactStrictMode: true,

  env: {
    API_URL: process.env.API_URL,
    IMAGES_DOMAIN: process.env.IMAGES_DOMAIN,
    REACT_IMAGE_BASE_URL: process.env.REACT_IMAGE_BASE_URL,
    MONGO_URI: process.env.MONGO_URI,
  },
  publicRuntimeConfig: {
    API_URL: process.env.API_URL,
    IMAGES_DOMAIN: process.env.IMAGES_DOMAIN,
  },

  images: {
    deviceSizes: [640, 768, 1024, 1280, 1600], // css breakpoints
    imageSizes: [16, 32, 48, 64, 96, 128],
    domains: [process.env.IMAGES_DOMAIN],
    path: "/_next/image",
    loader: "default",
  },
};
