# Blog

Blog write in NextJS with CMS in Strapi and database in MongoDB.

- payment option (Stripe)
- comment section
- emoji reactions on post (+ their count)
- search bar
- calendar with publication dates
- SEO optimization
- Bootstrap customized components
